<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>
<?php wp_footer(); ?>
<!-- bootstrap-datetimepicker -->
<script src="<?php echo THEME_URI ?>/js/bootstrap-datetimepicker.min.js"></script>
<script>
    jQuery('.myDatepicker3').datetimepicker({
        format: 'hh:mm A'
    });
</script>
</body>
</html>