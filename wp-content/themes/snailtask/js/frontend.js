function loading(sel){
	jQuery(sel).attr('disabled','disabled');
	jQuery(sel).attr('data-loading',jQuery(sel).html());
	jQuery(sel).html('<i class="spinner-submit"></i>');
}
function unloading(sel){
	var loading=jQuery(sel).attr('data-loading');
	jQuery(sel).html(loading);
	jQuery(sel).removeAttr('disabled');
}
function pad(num, size) {
    var s = num+"";
    while (s.length < size) s = "0" + s;
    return s;
}
function callAjaxEvent(status, data){
	dt = new Date(data.start.toISOString());
	mydate = dt.getFullYear()+'-'+pad((dt.getMonth()+1),2)+'-'+pad(dt.getDate(),2) + ' ' + pad(dt.getHours(),2) + ':' + pad(dt.getMinutes(),2) + ':' + pad(dt.getSeconds(),2);
	console.log(data.end);
	if(typeof data.end !== 'undefined' && data.end && data.end !== 'NULL'){
		dt = new Date(data.end.toISOString());
		myenddate = dt.getFullYear()+'-'+pad((dt.getMonth()+1),2)+'-'+pad(dt.getDate(),2) + ' ' + pad(dt.getHours(),2) + ':' + pad(dt.getMinutes(),2) + ':' + pad(dt.getSeconds(),2);
	}else{
		myenddate=mydate;
	}
	
	if(status=="new"){
		postbyurl('hide_me',hr.a_url+'?action=add_calendar','title='+data.title+'&customer='+data.customer+'&descr='+data.descr+'&start='+mydate+'&end='+myenddate);
		//jQuery('#calendar').fullCalendar('renderEvent', data,true);
	}else{			  
		postbyurl('hide_me',hr.a_url+'?action=update_calendar','id='+data.id+'&title='+data.title+'&customer='+data.customer+'&descr='+data.descr+'&start='+mydate+'&end='+myenddate);
		jQuery('#calendar').fullCalendar('updateEvent', data);	
	}
}

jQuery(document).ready(function(){
								
	jQuery('.login_form_submit').submit(function(){
		loading('.login_submit');
		postall_or('hide_me',hr.a_url+'?action=login','log_email||log_password||login_nonce');
	})
	
	jQuery('.form_search_employee').submit(function(){
		loading('.submit_search_employee');
		var key = jQuery("#key_employee").val();
		postbyurl('list_employee',hr.a_url+'?action=search_employee','key='+key);
	})
	
	
	jQuery('.add_task_form').submit(function(){
		loading('.addt_submit');
		postall_or('hide_me',hr.a_url+'?action=add_task','addt_start||addt_end||addt_name||addt_service||addt_price');
	})
	
	jQuery('.update_task_form').submit(function(){
		loading('.updatet_submit');
		postall_or('hide_me',hr.a_url+'?action=update_task','addt_id||addt_start||addt_end||addt_name||addt_service||addt_price');
	})
	
	jQuery('.click_list_employee').click(function(){
		jQuery('.click_list_employee').removeClass('active');
		jQuery(this).addClass('active');
		var page = jQuery(this).attr('data-page');
		window.history.pushState(null, null, hr.h_url+'/employees/?pg='+page);
		loading('.click_list_employee.active');
		postbyurl('list_employee',hr.a_url+'?action=load_employee','pg='+page);
	})
	
	jQuery('.btn_submit_profile').click(function(){
		loading('.btn_submit_profile');
		postall_or('hide_me',hr.a_url+'?action=update_employee','user_id_meta||st_user_avatar||user_name_meta||user_address_meta||user_phone_meta||user_password_meta||user_salary_meta||user_studio_meta');
	})
	
	jQuery(document).delegate(".click_paid", "click", function(){
		loading(this);
		var ids = jQuery(this).attr('data-id');
		var paid = jQuery(this).attr('data-value');
		postbyurl('hide_me',hr.a_url+'?action=click_paid','ids='+ids+'&paid='+paid);
	})
	
	jQuery(document).delegate(".add_service_button", "click", function(){
		loading(this);
		postall_or('hide_me',hr.a_url+'?action=add_service','services_title_meta||services_duration_meta||services_price_meta||services_studio_meta');
	})
	
	jQuery(document).delegate(".delete_service", "click", function(){
		if(confirm('Bạn có chắc chắn muốn xóa?')){
			loading(this);
			var id = jQuery(this).parents('tr').attr('data-id');
			postbyurl('hide_me',hr.a_url+'?action=delete_service','id='+id);
		}
	})
	
	jQuery(document).delegate(".update_service", "change", function(){
		var id = jQuery(this).parents('tr').attr('data-id');
		var title = jQuery(this).parents('tr').find('.services_title_meta').val();
		var duration = jQuery(this).parents('tr').find('.services_duration_meta').val();
		var price = jQuery(this).parents('tr').find('.services_price_meta').val();
		var studio = jQuery(this).parents('tr').find('.services_studio_meta').val();
		postbyurl('hide_me',hr.a_url+'?action=update_service','id='+id+'&services_title_meta='+title+'&services_duration_meta='+duration+'&services_price_meta='+price+'&services_studio_meta='+studio);
	})
	
	
	jQuery(document).delegate(".delete_user", "click", function(){
		if(confirm('Bạn có chắc chắn muốn xóa?')){
			loading(this);
			var id = jQuery(this).attr('data-id');
			postbyurl('hide_me',hr.a_url+'?action=delete_user','id='+id);
		}
	})
	
	jQuery(document).delegate(".add_employee_button", "click", function(){
		loading(this);
		postall_or('hide_me',hr.a_url+'?action=add_employee','user_name_meta||user_password_meta||user_fullname_meta||user_phone_meta||user_address_meta||user_salary_meta||user_studio_meta');
	})
	
})