<div class="col-md-4 col-sm-4 col-xs-12 profile_details">
	<div class="well profile_view">
		<div class="col-sm-12">
			<div class="left col-xs-7">
				<h2><?php echo $arr->display_name; ?></h2>
				<ul class="list-unstyled">
					<?php echo hr_render(get_the_author_meta( 'user_address_meta', $arr->ID ),'<li><i class="fa fa-map-marker"></i> ','</li>');?>
					<?php echo hr_render(get_the_author_meta( 'user_phone_meta', $arr->ID ),'<li><i class="fa fa fa-phone"></i> ','</li>');?>
				</ul>
			</div>
			<div class="right col-xs-5 text-center">
				<img src="<?php echo get_wp_user_avatar_src($arr->ID, 'medium'); ?>" alt="" class="img-circle img-responsive">
			</div>
		</div>
		<div class="col-xs-12 bottom text-center">
			<div class="col-xs-12 col-sm-4 emphasis"></div>
			<div class="col-xs-12 col-sm-4 emphasis">
				<a href="javascript:void(0);" class="delete_user" data-id="<?php echo $arr->ID; ?>">
					<button type="button" class="btn btn-danger btn-xs">
						<i class="fa fa-trash"> </i> Xóa Profile
					</button>
				</a>
			</div>
			<div class="col-xs-12 col-sm-4 emphasis">
				<a href="<?php echo get_page_link(get_page_by_path('user-profile')).'?uid='.$arr->ID ?>">
					<button type="button" class="btn btn-primary btn-xs">
						<i class="fa fa-user"> </i> Xem Profile
					</button>
				</a>
				
			</div>
		</div>
	</div>
</div>