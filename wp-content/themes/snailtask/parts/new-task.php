<div>
    <div class="login_wrapper">
        <div class="animate form add_task_wrap">
            <section class="add_task_content">
                <form class="add_task_form" onsubmit="return false">
                    <h1><?php echo change_time_l(current_time('N')).current_time(', d/m/Y'); ?></h1>
                    <div class="form-group">
                        <div class="input-group date myDatepicker3">
                            <input placeholder="Bắt đầu" type="text" class="form-control hr-no-event" id="addt_start">
                            <span class="input-group-addon">
                               <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="input-group date myDatepicker3">
                            <input placeholder="Kết thúc" type="text" class="form-control hr-no-event" id="addt_end">
                            <span class="input-group-addon">
                               <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="input-group">
                            <input placeholder="Tên khách" type="text" class="form-control" id="addt_name">
                        </div>
                    </div>
					
					<div class="form-group">
                        <div class="label-time">Dịch vụ</div>
                        <div class="input-group">
							<select name="addt_service" id="addt_service" class="form-control">
								<?php
								$arrs = get_posts(array('post_type'=>'services','posts_per_page'=>-1,'orderby'=>'menu_order','order'=>'ASC'));
								foreach($arrs as $arr){
									?>
									<option value="<?php echo $arr->ID; ?>"><?php echo $arr->post_title; ?></option>
									<?php
								}
								?>
							</select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="input-group">
                            <input  placeholder="$" type="text" class="form-control" id="addt_price">
                        </div>
                    </div>

                    <div class="button_form">
                        <a class="btn btn-primary history" href="<?php echo get_permalink( get_page_by_path( 'history' ) ) ?>">Xem lịch sử</a>
						<button class="btn btn-success submit addt_submit">Lưu lại</button>
                    </div>
                    <div class="clearfix"></div>
                </form>
            </section>
        </div>

    </div>
</div>