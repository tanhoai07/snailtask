<?php
$tid = $_GET['tid'];
$task = get_post($tid);
if(!(current_user_can('administrator') || $task->post_author == MYID)){
	?>
	<script>
		window.location = '<?php echo get_permalink(get_page_by_path('task')); ?>';
	</script>
	<?php
	exit;
}
?>
<div>
    <div class="login_wrapper">
        <div class="animate form add_task_wrap">
            <section class="add_task_content">
                <form class="update_task_form" onsubmit="return false">
                    <h1><?php echo change_time_l(date('N',strtotime(hr_meta($tid,'tasks_date_meta')))).date(', d.m.Y',strtotime(hr_meta($tid,'tasks_date_meta'))); ?></h1>
                    <div class="form-group">
                        <div class="input-group date myDatepicker3">
                            <input placeholder="Bắt đầu" type="text" class="form-control hr-no-event" id="addt_start" value="<?php echo hr_meta($tid,'tasks_start_meta'); ?>">
                            <span class="input-group-addon">
                               <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="input-group date myDatepicker3">
                            <input  placeholder="Kết thúc" type="text" class="form-control hr-no-event" id="addt_end" value="<?php echo hr_meta($tid,'tasks_end_meta'); ?>">
                            <span class="input-group-addon">
                               <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="input-group">
                            <input placeholder="Khách hàng" type="text" class="form-control" id="addt_name" value="<?php echo hr_meta($tid,'tasks_name_meta'); ?>">
                        </div>
                    </div>
					
					<div class="form-group">
                        <div class="label-time">Dịch vụ</div>
                        <div class="input-group">
							<select name="addt_service" id="addt_service" class="form-control">
								<?php
								$serviceid = hr_meta($tid,'tasks_service_meta');
								$arrs = get_posts(array('post_type'=>'services','posts_per_page'=>-1,'orderby'=>'menu_order','order'=>'ASC'));
								foreach($arrs as $arr){
									$selected=($arr->ID==$serviceid)?'selected="selected"':'';
									?>
									<option value="<?php echo $arr->ID; ?>" <?php echo $selected; ?>><?php echo $arr->post_title; ?></option>
									<?php
								}
								?>
							</select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="input-group">
                            <input placeholder="$" type="text" class="form-control" id="addt_price" value="<?php echo hr_meta($tid,'tasks_price_meta'); ?>">
                        </div>
                    </div>

                    <div class="button_form">
                        <a class="btn btn-primary history" href="<?php echo get_permalink( get_page_by_path( 'history' ) ) ?>">Xem lịch sử</a>
						<input type="hidden" id="addt_id" value="<?php echo $tid; ?>" />
						<button class="btn btn-success submit updatet_submit">Cập nhật</button>
                    </div>
                    <div class="clearfix"></div>
                </form>
            </section>
        </div>

    </div>
</div>