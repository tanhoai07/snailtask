<?php
if(!empty($arrs)){
	?>
	<table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
	  <thead>
		<tr>
		  <th>Tên</th>
		  <th>Thời gian làm việc</th>
		  <th>Tổng thu nhập</th>
		  <th>Lương</th>
		  <th>Doanh thu</th>
		  <th class="align-center">Thanh toán</th>
		</tr>
	  </thead>
	  <tbody>
		<?php
		$sum_total = 0;
		$sum_salary = 0;
		foreach($arrs as $uid=>$arrs_id){
			$uuser = get_user_by('id',$uid);
			$total = 0;
			$user_rate = (get_the_author_meta('user_salary_meta', $uid))?get_the_author_meta('user_salary_meta', $uid):0;
			?>
			<tr>
			  <td data-title="Tên"><?php echo $uuser->display_name; ?></td>
			  <td data-title="Thời gian làm việc">
				<table width="100%" class="sub-table-accounting">
					<?php
					foreach($arrs_id as $arr_id){
					$price = (hr_meta($arr_id,'tasks_price_meta'))?hr_meta($arr_id,'tasks_price_meta'):0;
					$total = $total + $price;
					?>
					<tr>
						<td><?php echo strtoupper(hr_meta($arr_id,'tasks_start_meta')); ?></td>
						<td><?php echo strtoupper(hr_meta($arr_id,'tasks_end_meta')); ?></td>
						<td>$<?php echo $price;?></td>
					</tr>
					<?php
					}
					?>
				</table>
			  </td>
			  <td data-title="Tổng thu nhập">$<?php echo $total; ?></td>
			  <td data-title="Lương">$<?php $salary = round($total*$user_rate,2); echo $salary; ?></td>
			  <td data-title="Doanh thu">$<?php echo ($total-$salary); ?></td>
			  <td class="align-center" data-title="Thanh toán">
			  	<?php $paid = hr_meta($arrs_id[0],'hr_paid'); ?>
				<a href="javascript:void(0)" data-id="<?php echo implode(',',$arrs_id); ?>" data-value="<?php echo $paid; ?>" class="<?php echo (is_hr_admin())?'click_paid':'';?> click_paid_css">
					<i class="fa <?php echo $paid?'fa-check-circle paid':'fa-money unpay'; ?>"></i>
				</a>
			  </td>
			</tr>
			<?php
			$sum_total = $sum_total + $total;
			$sum_salary = $sum_salary + $salary; 
		}
		?>
	  </tbody>
	  <tfoot>
		<tr>
		  <td colspan="2" style="text-align:center; font-weight:bold">Tổng cộng</td>
		  <td>$<?php echo round($sum_total,2); ?></td>
		  <td>$<?php echo round($sum_salary,2); ?></td>
		  <td>$<?php echo round($sum_total-$sum_salary,2); ?></td>
		  <td></td>
		</tr>
	  </tfoot>
	</table>
	<?php
}else{
?>
<div class="hr-not-found">Chưa có dữ liệu</div>
<?php
}
?>