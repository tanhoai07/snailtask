<?php
/** PREVENT UPDATE CHECK **/
add_filter( 'http_request_args', 'dm_prevent_update_check', 10, 2 );
function dm_prevent_update_check( $r, $url ) {
	if ( 0 === strpos( $url, 'http://api.wordpress.org/plugins/update-check/' ) ) {
		$my_plugin = plugin_basename( __FILE__ );
		$plugins = unserialize( $r['body']['plugins'] );
		unset( $plugins->plugins[$my_plugin] );
		unset( $plugins->active[array_search( $my_plugin, $plugins->active )] );
		$r['body']['plugins'] = serialize( $plugins );
	}
	return $r;
}

/** REMOVE DEFAULT IMAGE SIZE **/
add_filter('intermediate_image_sizes_advanced', 'hero_remove_default_image_sizes');
function hero_remove_default_image_sizes( $sizes) {
	unset( $sizes['thumbnail']);
	//unset( $sizes['medium']);
	unset( $sizes['large']);
	unset( $sizes['medium_large']);
	return $sizes;
}

/** FILTER DEFAUTL AVATA **/
add_filter( 'get_avatar' , 'hr_custom_avatar' , 1 , 6 );
function hr_custom_avatar( $avatar, $id_or_email, $size, $default, $alt) {
	$user=get_user_by('id',$id_or_email);
	$url=(isset($user->ID))?get_user_meta($user->ID,'avata_meta',true):'';
	$url=($url)?UPLOAD_URI.'/avatas/'.$url:THEME_URI.'/images/user.png';
	$avata='<img alt="'.$alt.'" src="'.$url.'" class="avatar avatar-'.$size.' photo" height="'.$size.'" width="'.$size.'" />';
	return $avata;
}

/* --- custom new user -----*/
function new_modify_user_table( $column ) {
	$column['studio'] = 'Studio';
	return $column;
}
add_filter( 'manage_users_columns', 'new_modify_user_table' );
function new_modify_user_table_row( $val, $column_name, $user_id ) {
	switch ($column_name) {
		case 'studio' :
			$studio = get_the_author_meta( 'user_studio_meta', $user_id );
			return ($studio)?get_post($studio)->post_title:'';
			break;
		default:
	}
	return $val;
}
add_filter( 'manage_users_custom_column', 'new_modify_user_table_row', 10, 3 );

/** add class body **/
add_filter( 'body_class', 'custom_class' );
function custom_class( $classes ) {
    $classes[] = 'nav-md';
    return $classes;
}
?>