<?php
/***** ADD SESSION ******/
add_action('init', 'hr_start_session', 1);
function hr_start_session() {
	if(!session_id()) {
		session_start();
	}
}
/**** REMOVE EMOJI *****/
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );

/**** REMOVE CORE UPDATE *****/
add_action('after_setup_theme','hr_remove_core_updates');
function hr_remove_core_updates(){
  if(! current_user_can('update_core')){return;}
  	add_action('init', create_function('$a',"remove_action( 'init', 'wp_version_check' );"),2);
 	 add_filter('pre_option_update_core','__return_null');
  	add_filter('pre_site_transient_update_core','__return_null');
}
	
/**** REMOVE ADMIN BAR *****/	 
add_action('after_setup_theme', 'hr_remove_admin_bar');
function hr_remove_admin_bar() {
	if (!current_user_can('administrator') && !is_admin()) {
	  show_admin_bar(false);
	}
}
		
/**** BLOCK USER ACESS ADMIN  *****/
add_action( 'init', 'hr_blockusers_init' );
function hr_blockusers_init() {
	if ( is_admin() && ! current_user_can( 'administrator' ) &&
	! ( defined( 'DOING_AJAX' ) && DOING_AJAX ) && is_user_logged_in() ) {
		wp_redirect( home_url() );
		exit;
	}
}

/***** ADD SCRIPT FRONTEND ******/
add_action('wp_enqueue_scripts', 'hr_frontend_script');
function hr_frontend_script(){
	//wp_enqueue_script('underscore');

    wp_enqueue_script('jquery', THEME_URI.'/js/jquery.min.js',array('jquery'),'', true);
    wp_enqueue_script('bootstrap', THEME_URI.'/js/bootstrap.min.js',array('jquery'),'', true);
//    wp_enqueue_script('fastclick', THEME_URI.'/master/vendors/fastclick/lib/fastclick.js',array('jquery'),'', true);
    wp_enqueue_script('Chart', THEME_URI.'/js/Chart.min.js',array('jquery'),'', true);
    wp_enqueue_script('date', THEME_URI.'/js/date.js',array('jquery'),'', true);
    wp_enqueue_script('flot', THEME_URI.'/js/jquery.flot.js',array('jquery'),'', true);
    wp_enqueue_script('flot pie', THEME_URI.'/js/jquery.flot.pie.js',array('jquery'),'', true);
    wp_enqueue_script('flot time', THEME_URI.'/js/jquery.flot.time.js',array('jquery'),'', true);

    wp_enqueue_script('moment', THEME_URI.'/js/moment.min.js',array('jquery'),'', true);
    wp_enqueue_script('fullcalendar', THEME_URI.'/js/fullcalendar.min.js',array('jquery'),'', true);
    wp_enqueue_script('custom', THEME_URI.'/js/custom.js',array('jquery'),'', true);

    wp_enqueue_script('labory', THEME_URI.'/js/labory.js',array('jquery'),'', true);
    wp_localize_script('labory', 'hr', array('p_url' => THEME_URI,'a_url'=>AJAX_URI,'h_url'=>home_url()));
    wp_enqueue_script('frontend-js', THEME_URI.'/js/frontend.js',array('jquery'),'',filemtime(THEME_PATH.'/js/frontend.js'), true);
    wp_localize_script('frontend-js', 'hr', array('p_url' => THEME_URI,'a_url'=>AJAX_URI,'h_url'=>home_url()));
	wp_enqueue_script('notice-lib', THEME_URI.'/js/notice_lib.js',array('jquery'),'', true);
	wp_enqueue_script('notice', THEME_URI.'/js/notice.js',array('jquery'),'', true);
    wp_enqueue_style('frontend-css', THEME_URI.'/css/frontend.css','',filemtime(THEME_PATH.'/css/frontend.css'));
}
/***** ADD SCRIPT BACKEND ******/
add_action('admin_enqueue_scripts', 'hr_admin_script');
function hr_admin_script() {
	wp_enqueue_style('backendcss', THEME_URI.'/css/backend.css');
	wp_enqueue_script('backendjs', THEME_URI.'/js/backend.js',array(),'', true);
	wp_localize_script('backendjs', 'hr', array('p_url' => THEME_URI,'a_url'=>AJAX_URI));
	wp_enqueue_style('choosencss', THEME_URI.'/css/chosen.css');
	wp_enqueue_script('choosenjs', THEME_URI.'/js/chosen.jquery.js',array(),'', true);
}

/*****ADD ACTION INIT****/
add_action('hr_action_init','hr_action_init');
function hr_action_init(){
	if(!is_user_logged_in()){
		$_SESSION['noacess']='http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
		wp_redirect(get_page_link(get_page_by_path('login')));
		exit;
	}
	if(!is_hr_admin() && (is_page_template('templates/calendar.php') || is_page_template('templates/home.php') || is_page_template('templates/employees.php'))){
		wp_redirect(get_page_link(get_page_by_path('user-profile')));
	}
	return true;
}

/*****ADD ACTION INIT****/
add_action('hr_action_init_login','hr_action_init_login');
function hr_action_init_login(){
	if(is_user_logged_in()){
		wp_redirect(home_url());
		exit;
	}
	return true;
}

/***** ADD SCRIPT BACKEND ******/
add_action( 'wp_footer', 'hr_datas',100 );
function hr_datas(){
	ob_start();
	?>
	<div style="display:none"><div id="hide_me"></div></div>
	<?php
	if(is_page_template('templates/home.php') || 1){
		$datas=array();
		for($i=20; $i>=0; $i--){
			$date = NOW-3600*24*$i;
			$amount = get_amounts(date('Y-m-d',$date));
			$datas[]=array($date*1000,($amount==0)?10:$amount);
		}
		?>
		<script id="chart_plot_02_data" type="data/json"><?php echo json_encode($datas); ?></script>
		<?php
		$arrs = get_top_service();
		$datas = array();
		if(!empty($arrs)){
			$amount_total = get_amounts(DAY);
			$labels=$percent=array();
			$count=0;
			foreach($arrs as $serviceid => $amount){
				$count++;
				if($count>5){ break; }
				$labels[]=get_the_title($serviceid);
				$percent[]=round($amount*100/$amount_total,2);
			}
			$datas=array(
				'labels'=>$labels,
				'percent' => $percent,
				'backgroundColor'=>array("#BDC3C7","#9B59B6","#E74C3C","#26B99A","#3498DB"),
				'hoverBackgroundColor'=>array("#CFD4D8","#B370CF","#E95E4F","#36CAAB","#49A9EA")
			);
		}
		?>
		<script id="chart_doughnut_data" type="data/json"><?php echo json_encode($datas); ?></script>
		<?php		
	}
	
	/****/
	if(is_page_template('templates/profile.php')){
		$uid = (is_hr_admin() && isset($_GET['uid']) && $_GET['uid'])?$_GET['uid']:MYID;
		$datas=array();
		$arrs = get_tasks_employee($uid);
		$user_rate = (get_the_author_meta('user_salary_meta', $uid))?get_the_author_meta('user_salary_meta', $uid):0;
		if(!empty($arrs)){
			foreach($arrs as $date=>$amount){
				$datas[]=array('date'=>$date,'amount'=>round($amount*$user_rate,2));
			}
		}
		?>
		<script id="morris_charts_data" type="data/json"><?php echo json_encode($datas); ?></script>
		<?php	
	}
	
	/*** data for calendar ***/
	if(is_page_template('templates/calendar.php')){
		$arrs = get_posts(array('post_type'=>'calendar','posts_per_page'=>-1));
		$datas=array();
		if(!empty($arrs)){
			foreach($arrs as $arr){
				if($arr->post_title){
					$datas[]=array('id'=>$arr->ID,'title'=>$arr->post_title,'customer'=>hr_meta($arr->ID,'calendar_customer_meta'),'descr'=>hr_meta($arr->ID,'calendar_desrc_meta'),'start'=>hr_meta($arr->ID,'calendar_date_meta'),'end'=>hr_meta($arr->ID,'calendar_end_meta'));
				}	
			}
		}
		
		?>
		<script id="calendar_data" type="data/json"><?php echo json_encode($datas); ?></script>
		<?php	
	}
		
	echo  ob_get_clean();
}
/*** REGISTRY POSTTYPE ***/
add_action('init','register_post_type_func');
function register_post_type_func(){
	$arrs=array(
		array(
			'name'		=>	'Studios',
			'slug'		=>	'studios',
			'support'	=>	array('title','author'),
			'icon'		=>	'dashicons-store'
		),
		array(
			'name'		=>	'Services',
			'slug'		=>	'services',
			'support'	=>	array('title','author'),
			'icon'		=>	'dashicons-admin-tools'
		),
		array(
			'name'		=>	'Tasks',
			'slug'		=>	'tasks',
			'support'	=>	array('title','author'),
			'icon'		=>	'dashicons-clipboard'
		),
		array(
			'name'		=>	'Calendar',
			'slug'		=>	'calendar',
			'support'	=>	array('title','author'),
			'icon'		=>	'dashicons-calendar'
		),
	);
	foreach($arrs as $arr){
		create_post_type($arr);
	}
}

/*** REGISTRY POSTMETA ***/
add_action('init','register_post_meta_func');
function register_post_meta_func(){
	$arrs = array('studios','tasks','services','calendar');
	foreach($arrs as $arr){
		hr_get_post_meta($arr);
	}
}

/*** CHANGE ROLE NAME ***/
function change_role_name() {
	global $wp_roles;	
	if ( ! isset( $wp_roles ) )
	$wp_roles = new WP_Roles(); 
	$wp_roles->roles['editor']['name'] = 'Manager';
	$wp_roles->role_names['editor'] = 'Manager';
	$wp_roles->roles['author']['name'] = 'Employees';
	$wp_roles->role_names['author'] = 'Employees'; 
	
	if( get_role('subscriber') ){
		  remove_role( 'subscriber' );
	}
	if( get_role('contributor') ){
		  remove_role( 'contributor' );
	}
}
add_action('init', 'change_role_name');


add_action( 'show_user_profile', 'my_user_profile_fields' );
add_action( 'edit_user_profile', 'my_user_profile_fields' );
function my_user_profile_fields( $user ) {
$studio = esc_attr(get_the_author_meta( 'user_studio_meta', $user->ID ));
$arrs = get_posts(array('post_type'=>'studios','posts_per_page'=>-1,'orderby'=>'menu_order','order'=>'ASC'));
?>
  <table class="form-table">		
		<tr>
		  <th><label for="phone"><?php _e("Address"); ?></label></th>
		  <td><input type="text" name="user_address_meta" id="user_address_meta" value="<?php echo esc_attr(get_the_author_meta( 'user_address_meta', $user->ID )); ?>" class="regular-text ltr"></td>
		</tr>
		<tr>
		  <th><label for="phone"><?php _e("Phone"); ?></label></th>
		  <td><input type="text" name="user_phone_meta" id="user_phone_meta" value="<?php echo esc_attr(get_the_author_meta( 'user_phone_meta', $user->ID )); ?>" class="regular-text ltr"></td>
		</tr>
		<tr>
		  <th><label for="phone"><?php _e("Tỷ lệ ăn chia"); ?></label></th>
		  <td>
		  	<select name="user_salary_meta" class="regular-text">
				<?php
				$rates = get_share_rating();
				$myval = get_the_author_meta( 'user_salary_meta', $user->ID );
				foreach($rates as $val => $name){
					$selected = ($val==$myval)?'selected="selected"':'';
					?>
					<option value="<?php echo $val; ?>" <?php echo $selected; ?>><?php echo $name; ?></option>
					<?php
				}
				?>
			</select>
		</tr>
		<tr>
		  <th><label for="phone"><?php _e("Studio"); ?></label></th>
		  <td>
			<select name="user_studio_meta" class="regular-text">
				<?php
				foreach($arrs as $arr){
					$selected = ($arr->ID==$studio)?'selected="selected"':'';
					?>
					<option value="<?php echo $arr->ID; ?>" <?php echo $selected; ?>><?php echo $arr->post_title; ?></option>
					<?php
				}
				?>
			</select>
		  </td>
		 </tr>
  </table>
<?php
}

add_action( 'personal_options_update', 'my_save_user_profile_fields' );
add_action( 'edit_user_profile_update', 'my_save_user_profile_fields' );
function my_save_user_profile_fields( $user_id ) {
  $saved = false;
  if ( current_user_can( 'edit_user', $user_id ) ) {
	$metas=array('user_address_meta','user_phone_meta','user_salary_meta','user_studio_meta');
	foreach($metas as $meta){
		update_user_meta($user_id, $meta, $_POST[$meta]);
	}
	
	$saved = true;
  }
  return true;
}

/***** DISABLE COMMENT ***/

// Disable support for comments and trackbacks in post types
add_action('admin_init', 'df_disable_comments_post_types_support');
function df_disable_comments_post_types_support() {
	$post_types = get_post_types();
	foreach ($post_types as $post_type) {
		if(post_type_supports($post_type, 'comments')) {
			remove_post_type_support($post_type, 'comments');
			remove_post_type_support($post_type, 'trackbacks');
		}
	}
}
// Close comments on the front-end
add_filter('comments_open', 'df_disable_comments_status', 20, 2);
add_filter('pings_open', 'df_disable_comments_status', 20, 2);
function df_disable_comments_status() {
	return false;
}
// Hide existing comments
add_filter('comments_array', 'df_disable_comments_hide_existing_comments', 10, 2);
function df_disable_comments_hide_existing_comments($comments) {
	$comments = array();
	return $comments;
}
// Remove comments page in menu
add_action('admin_menu', 'df_disable_comments_admin_menu');
function df_disable_comments_admin_menu() {
	remove_menu_page('edit-comments.php');
}
// Redirect any user trying to access comments page
add_action('admin_init', 'df_disable_comments_admin_menu_redirect');
function df_disable_comments_admin_menu_redirect() {
	global $pagenow;
	if ($pagenow === 'edit-comments.php') {
		wp_redirect(admin_url()); exit;
	}
}
// Remove comments metabox from dashboard
add_action('admin_init', 'df_disable_comments_dashboard');
function df_disable_comments_dashboard() {
	remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');
}
// Remove comments links from admin bar
add_action('init', 'df_disable_comments_admin_bar');
function df_disable_comments_admin_bar() {
	if (is_admin_bar_showing()) {
		remove_action('admin_bar_menu', 'wp_admin_bar_comments_menu', 60);
	}
}

/***** REMOVE FIELD IN PROFILE USER *****/

// Removes admin color scheme options
remove_action( 'admin_color_scheme_picker', 'admin_color_scheme_picker' );

// Removes the leftover 'Visual Editor', 'Keyboard Shortcuts' and 'Toolbar' options.
add_action( 'admin_head', 'cor_profile_subject_start' );
add_action( 'admin_footer', 'cor_profile_subject_end' );
if ( ! function_exists( 'cor_remove_personal_options' ) ) {
	function cor_remove_personal_options( $subject ) {
		$subject = preg_replace( '#<h2>Personal Options</h2>.+?/table>#s', '', $subject, 1 );
		$subject = preg_replace( '#<h2>About Yourself</h2>#s', '', $subject, 1 );
		$subject = preg_replace('#<tr class="user-description-wrap(.*?)</tr>#s', '', $subject, 1);
		$subject = preg_replace( '#<h2>About the user</h2>.+?/table>#s', '', $subject, 1 );
		$subject = preg_replace('#<h2>'.__("Contact Info").'</h2>#s', '', $subject, 1);
		$subject = preg_replace('#<h2>'.__("Name").'</h2>#s', '', $subject, 1);
		$subject = preg_replace('#<h2>'.__("Account Management").'</h2>#s', '', $subject, 1);
		$subject = preg_replace('#<tr class="user-url-wrap(.*?)</tr>#s', '', $subject, 1);
		//$subject = preg_replace('#<tr class="user-display-name-wrap(.*?)</tr>#s', '', $subject, 1);
		return $subject;
	}

	function cor_profile_subject_start() {
		ob_start( 'cor_remove_personal_options' );
	}

	function cor_profile_subject_end() {
		ob_end_flush();
	}
}

add_action( 'admin_head-user-edit.php', 'hide_nickname_in_profile' );
add_action( 'admin_head-profile.php',   'hide_nickname_in_profile' );
function hide_nickname_in_profile()
{
	echo '<style>tr.user-nickname-wrap{ display: none; }</style>';
}
?>