<?php
	$upload_folder= wp_upload_dir();	
	define('THEME_URI', get_template_directory_uri());
	define('THEME_PATH', get_template_directory());
	define('AJAX_URI', admin_url('admin-ajax.php'));
	define('UPLOAD_URI', $upload_folder['baseurl']);
	define('UPLOAD_DIR', $upload_folder['basedir']);
	define('MYID',get_current_user_id());
	define('NOW',current_time('timestamp'));
	define('DAY',date('Y-m-d',current_time('timestamp')));
		
	add_theme_support( 'post-thumbnails' );
	
	require get_template_directory() . '/function/filters.php';
	require get_template_directory() . '/function/actions.php';
	require get_template_directory() . '/function/ajaxs.php';
	require get_template_directory() . '/function/shortcodes.php';
	require get_template_directory() . '/function/cropt.php';
	/*------------------all function here------------*/
	function hr_cropt($media_id,$w,$h){
		if(is_numeric($w) and is_numeric($h)){
			$url = wp_get_attachment_url($media_id);
			$ext = substr(strrchr($url, '/'),1);
			$cropt=UPLOAD_URI.'/cropt/';
			if($url=="") return $url;
			if(file_exists($cropt.$media_id."_".$w."x".$h."_".$ext)){	
				$url=$cropt.$media_id."_".$w."x".$h."_".$ext;
			}
			else{
				$resizeObj = new resize($url);
				$resizeObj -> resizeImage($w,$h, "crop");
				$resizeObj -> saveImage(UPLOAD_DIR.'/cropt/'.$media_id."_".$w."x".$h."_".$ext, 100);
				$url=$cropt.$media_id."_".$w."x".$h."_".$ext;
			}
			return $url;
		}
		return "";
	}
	
	/** unsing str **/
	function hr_unsign($str){
       $arrs = array(
           'a'=>'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ',
           'd'=>'đ',
           'e'=>'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ',
           'i'=>'í|ì|ỉ|ĩ|ị',
           'o'=>'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ',
           'u'=>'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự',
           'y'=>'ý|ỳ|ỷ|ỹ|ỵ',
           'A'=>'Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',
           'D'=>'Đ',
           'E'=>'É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',
           'I'=>'Í|Ì|Ỉ|Ĩ|Ị',
           'O'=>'Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',
           'U'=>'Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',
           'Y'=>'Ý|Ỳ|Ỷ|Ỹ|Ỵ',
       );
      foreach($arrs as $val=>$name){
           $str = preg_replace("/($name)/i", $val, $str);
      }
     return $str;
   }
   /*------------mes--*/
	function mes($text, $color){
		ob_start();
		$color=($color==1)?'green':'red';
		?>
			<font color="<?php echo $color; ?>"><?php echo $text; ?></font>
		<?php
		return ob_get_clean();
	}
	/*------------notice--*/
	function add_notice($text, $color, $unload=''){
		ob_start();
		$color=($color==1)?'green':'red';
		?>
		<script>
			add_notice('<font color="<?php echo $color; ?>"><?php echo htmlspecialchars($text,ENT_QUOTES); ?></font>');
			<?php
			if($unload){
			?>
			unloading('<?php echo $unload; ?>');
			<?php
			}
			?>
		</script>
		<?php
		echo ob_get_clean();
	}
	
	/*---rewrite function var_dump--*/
	function var_dumps($object) {
		var_dump('<pre>',$object,'</pre>');
	}
	
	/** update json user **/
	function update_json_user($id,$key,$value,$notexist=true){
		$datas=get_user_meta($id,$key,true);
		$datas=($datas=="" or $datas=="[]")?array():(array)json_decode($datas);
		if( !( $notexist==true && in_array($value,$datas) ) ){
			$datas[]=$value;
		}
		return update_user_meta($id,$key,json_encode($datas));
	}
	
	/*----- login within password ---*/
	function login_within_pass($user_login,$user_pass,$remember=true){
		$creds = array();
		$creds['user_login'] = $user_login;
		$creds['user_password'] = $user_pass;	
		$creds['remember'] = $remember;
		return wp_signon( $creds, false ); 
	}

	/*------------------Create Menu------------------*/
	register_nav_menus( array(
		'main_menu' => 'Main Menu',
	) );
		
	/*** ----- registry posttype ***/
	function create_post_type($arrs)
    {
		$args = array(
            'labels' => array(
				'name' => __( $arrs['name'], 'snailtask'),
				'singular_name' => __( $arrs['name'], 'snailtask'),
				'add_new' => __( 'Add New', 'snailtask'),
				'add_new_item' => __( 'Add New '.$arrs['name'], 'snailtask' ),
				'edit_item' => __( 'Edit '.$arrs['name'], 'snailtask' ),
				'new_item' => __( 'New '.$arrs['name'], 'snailtask' ),
				'all_items' => __( 'All '.$arrs['name'], 'snailtask' ),
				'view_item' => __( 'View '.$arrs['name'], 'snailtask' ),
				'search_items' => __( 'Search '.$arrs['name'], 'snailtask' ),
				'not_found' => __( 'No '.$arrs['name'].' found', 'snailtask' ),
				'not_found_in_trash' => __( 'No '.$arrs['name'].' found in Trash', 'snailtask' ),
				'parent_item_colon' => '',
				'menu_name' => __( $arrs['name'], 'snailtask' ),
            ),
			'public' => true,
			'publicly_queryable' => true,
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => true,
			'capability_type' => 'post',
			'hierarchical' => false,
			'menu_position' => 7,
			'supports'=>(isset($arrs['support']))?$arrs['support']:array('title','author'),
			'menu_icon' => (isset($arrs['icon']))?$arrs['icon']:'',
        );
        register_post_type($arrs['slug'],$args);
    }
	
	
	/*** hr_get_post_meta ***/
	function hr_get_post_meta($slug){
		$file = THEME_PATH . '/function/metas/'.$slug.'.php';
		if(file_exists($file)){
			require_once $file;
		}
	}
	
	/*** hr_meta ***/
	function hr_meta($postid,$meta){
		return get_post_meta($postid,$meta,true);
	}
	
	/*** get amount from day to day **/
	function get_amounts($from,$to=''){
		$args = array(
			'post_type'=>'tasks',
			'posts_per_page'=>-1,
			'meta_query' => array(
				'relation' => 'AND',
				array(
					'key' => 'tasks_date_meta',
					'value' => $from,
					'compare' => '>=',
					'type'    => 'DATE'
				),
				array(
					'key' => 'tasks_date_meta',
					'value' => ($to)?$to:$from,
					'compare' => '<=',
					'type'    => 'DATE'
				), 
			),
		);
		$arrs = get_posts($args);
		$sum=0;
		foreach($arrs as $arr){
			$val = (get_post_meta($arr->ID,'tasks_price_meta',true))?get_post_meta($arr->ID,'tasks_price_meta',true):0;
			$sum=$sum+$val;
		}
		return round($sum,2);
	}
	
	/* get % mount abs */
	
	function get_percent_abs($from,$to){
		return abs(100-floor($to*100/$from));
	}
	function the_percent_abs($from,$to,$text=''){
		if($from<=0){
			return '';
		}
		if($from>$to){
			return '<i class="red"><i class="fa fa-sort-desc"></i>'.get_percent_abs($from,$to).'% </i>'.$text;
		}
		else{
			return '<i class="green"><i class="fa fa-sort-asc"></i>'.get_percent_abs($from,$to).'% </i>'.$text;
		}
		return true;
	}
	
	/* week */
	function get_current_week($minus=0){
		$start = NOW - 3600*24*(date('w',NOW) - $minus*7);
		return array(date('Y-m-d',$start),date('Y-m-d',$start + 3600*24*6));
	}
	/** month **/
	function get_current_month($minus=0){
		$start = date('Y-m-01',NOW);
		$end = date('Y-m-31',NOW);
		if($minus==-1){	
			$end = date('Y-m-d',(strtotime($start) - 3600*24));
			$start = date('Y-m-01',(strtotime($start) - 3600*24));	
		}
		return array($start,$end);
	}
	/** year **/
	function get_current_year($minus=0){
		$start = date('Y-01-01',NOW);
		$end = date('Y-12-31',NOW);
		if($minus==-1){	
			$end = date('Y-m-d',(strtotime($start) - 3600*24));
			$start = date('Y-01-01',(strtotime($start) - 3600*24));	
		}
		return array($start,$end);
	}
	/****/
	function get_top_employee(){
		$args = array(
			'post_type'=>'tasks',
			'posts_per_page'=>-1,
			'meta_query' => array(
				array(
					'key' => 'tasks_date_meta',
					'value' => DAY,
					'compare' => '=',
					'type'    => 'DATE'
				),
			),
		);
		$arrs = get_posts($args);
		$datas=array();
		foreach($arrs as $arr){
			$price = hr_meta($arr->ID,'tasks_price_meta');
			$price = ($price)?$price:0;
			$datas[$arr->post_author]=(isset($datas[$arr->post_author]))?($datas[$arr->post_author]+$price):$price;

		}
		arsort($datas);
		return $datas;
	}
	/*** get employee by date ***/
	function get_employee_by_date($date = DAY){
		$args = array(
			'post_type'=>'tasks',
			'posts_per_page'=>-1,
			'meta_query' => array(
				array(
					'key' => 'tasks_date_meta',
					'value' => $date,
					'compare' => '=',
					'type'    => 'DATE'
				),
			),
		);
		$caps = get_user_by('id',MYID);
		if($caps->roles[0]=='author'){
			$args['author']=MYID;
		}
		$arrs = get_posts($args);
		$datas=array();
		foreach($arrs as $arr){
			$datas[$arr->post_author][]=$arr->ID;
		}
		arsort($datas);
		return $datas;
	}
	/**  get top service **/
	function get_top_service(){
		$args = array(
			'post_type'=>'tasks',
			'posts_per_page'=>-1,
			'meta_query' => array(
				array(
					'key' => 'tasks_date_meta',
					'value' => DAY,
					'compare' => '=',
					'type'    => 'DATE'
				),
			),
		);
		$arrs = get_posts($args);
		$datas=array();
		foreach($arrs as $arr){
			$service = hr_meta($arr->ID,'tasks_service_meta');
			$price = hr_meta($arr->ID,'tasks_price_meta');
			$price = ($price)?$price:0;
			$datas[$service]=(isset($datas[$service]))?($datas[$service]+$price):$price;

		}
		arsort($datas);
		return $datas;
	}
	/** display name **/
	function get_name_employee($uid){
		$user = get_user_by('id',$uid);
		return $user->display_name;
	}
	/*** get color **/
	function get_color(){
		return array('blue','green','purple','aero','red');
	}
	
	/* render **/
	function hr_render($content,$before='',$after=''){
		return ($content)?$before.$content.$after:'';
	}
	/*****/
	function get_tasks_employee($uid,$num=30){
		$month = get_current_month();
		$args = array(
			'post_type'=>'tasks',
			'posts_per_page'=>$num,
			'author'=>$uid,
			'orderby'         	=> 'meta_value',
			'meta_key'        	=> 'tasks_date_meta',
			'order'			=> 'ASC',
			'meta_query' => array(
				'relation' => 'AND',
				array(
					'key' => 'tasks_date_meta',
					'value' => $month[0],
					'compare' => '>=',
					'type'    => 'DATE'
				),
				array(
					'key' => 'tasks_date_meta',
					'value' => $month[1],
					'compare' => '<=',
					'type'    => 'DATE'
				),
			),
		);
		$arrs = get_posts($args);
		$datas=array();
		if(!empty($arrs)){
			foreach($arrs as $arr){
				$hrdate = hr_meta($arr->ID,'tasks_date_meta');
				$price = hr_meta($arr->ID,'tasks_price_meta');
				$datas[$hrdate]=(isset($datas[$hrdate]))?($datas[$hrdate]+$price):$price;
			}
		}
		return $datas;
	}
	
	/******/
	/*-----------------get thumbnail id-------------------*/
	function get_attach_thumb_id($imageUrl, $post_id) {
			// Get the file name
			$filename = substr($imageUrl, (strrpos($imageUrl, '/'))+1);
			if (!(($uploads = wp_upload_dir(current_time('mysql')) ) && false === $uploads['error'])) {
				return null;
			}
			// Generate unique file name
			$filename = wp_unique_filename( $uploads['path'], $filename );
			// Move the file to the uploads dir
			$new_file = $uploads['path'] . "/$filename";
			if (!ini_get('allow_url_fopen')) {
				$file_data = curl_get_file_contents($imageUrl);
			} else {
				$file_data = @file_get_contents($imageUrl);
			}
			if (!$file_data) {
				return null;
			}
			file_put_contents($new_file, $file_data);
			// Set correct file permissions
			$stat = stat( dirname( $new_file ));
			$perms = $stat['mode'] & 0000666;
			@ chmod( $new_file, $perms );
			// Get the file type. Must to use it as a post thumbnail.
			$wp_filetype = wp_check_filetype( $filename, $mimes=null );
			extract( $wp_filetype );
			// No file type! No point to proceed further
			if ( ( !$type || !$ext ) && !current_user_can( 'unfiltered_upload' ) ) {
				return null;
			}
			// Compute the URL
			$url = $uploads['url'] . "/$filename";
			// Construct the attachment array
			$attachment = array(
				'post_mime_type' => $type,
				'guid' => $url,
				'post_parent' => null,
				'post_title' => '',
				'post_content' => '',
			);
			$thumb_id = wp_insert_attachment($attachment, $filename, $post_id);
			if ( !is_wp_error($thumb_id) ) {
				require_once(ABSPATH . '/wp-admin/includes/image.php');
		
				// Added fix by misthero as suggested
				wp_update_attachment_metadata( $thumb_id, wp_generate_attachment_metadata( $thumb_id, $new_file ) );
				update_attached_file( $thumb_id, $new_file );
				return $thumb_id;
			}
			return null;
	}
	
	/******* get share_rating ****/
	function get_share_rating(){
		return array('0'=>'Lương cứng','0.4'=>'4/6','0.5'=>'5/5','0.6'=>'6/4');
	}
	/****** */
	function is_hr_admin(){
		$caps = get_user_by('id',MYID);
		return ($caps->roles[0]=='editor' || $caps->roles[0]=='administrator');
	}
	/* change time **/
	
	function change_time_l($int){
		if($int==7){
			return 'Chủ nhật';
		}else{
			return 'Thứ '.($int+1);
		}
	}
?>