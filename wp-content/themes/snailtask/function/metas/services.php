<?php
/*---- add show home--*/
add_action('add_meta_boxes', 'add_services_meta');
function add_services_meta() {
	add_meta_box('Option', 'Option', 'show_services_meta', 'services');
}
function show_services_meta() {
	global $post;
	echo '<input type="hidden" name="services_meta_nonce" value= "' . wp_create_nonce(basename(__FILE__)) . '"/>';
?>
<div class="option option-services">
	<div class="option-item">
		<label>Duration</label>
		<input type="text" name="services_duration_meta" value="<?php echo hr_meta($post->ID,'services_duration_meta'); ?>" />
	</div>
	<div class="option-item">
		<label>Price</label>
		<input type="text" name="services_price_meta" value="<?php echo hr_meta($post->ID,'services_price_meta'); ?>" />
	</div>
	<div class="option-item">
		<label>Studio</label>
		<input type="text" name="services_studio_meta" id="services_studio_meta" value="<?php echo hr_meta($post->ID,'services_studio_meta'); ?>"  />
	</div>
	
	<div class="clearboth"></div>
</div>
<?php	
}

/** SAVE POSTYPE **/	
add_action('save_post', 'save_services_meta');
function save_services_meta($post_id) {
	global $custom_meta_fields;
	// verify nonce  
	if(isset($_POST['services_meta_nonce'])){
		if (!wp_verify_nonce($_POST['services_meta_nonce'], basename(__FILE__))){
			return $post_id;
		}
		$metas=array('services_duration_meta','services_price_meta','services_studio_meta');
		foreach($metas as $meta){
			update_post_meta($post_id, $meta, $_POST[$meta]);
		}
	}
}

/** ADD COLUMN POSTYPE **/

/*---noi bat---*/
add_filter('manage_services_posts_columns', 'add_colum_services');
function add_colum_services($defaults) {
	unset($defaults['date']);
	unset($defaults['author']);
	$defaults['services_duration_meta'] = 'Duration';
	$defaults['services_price_meta'] = 'Price';
	$defaults['services_studio_meta'] = 'Studio';
	$defaults['date'] = 'Date';
	return $defaults;
}
add_action('manage_services_posts_custom_column', 'add_content_colum_services', 10, 2);
function add_content_colum_services($column_name, $postid) {
	switch ($column_name) {
		case 'services_duration_meta':
		case 'services_price_meta':
		case 'services_studio_meta':
			echo hr_meta($postid,$column_name);
			break;
	}
}
?>