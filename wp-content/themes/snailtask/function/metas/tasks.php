<?php
/*---- add show home--*/
add_action('add_meta_boxes', 'add_tasks_meta');
function add_tasks_meta() {
	add_meta_box('Option', 'Option', 'show_tasks_meta', 'tasks');
}
function show_tasks_meta() {
	global $post;
	echo '<input type="hidden" name="tasks_meta_nonce" value= "' . wp_create_nonce(basename(__FILE__)) . '"/>';
?>
<div class="option option-tasks">
	<div class="option-item">
		<label>Date</label>
		<input type="text" name="tasks_date_meta" value="<?php echo hr_meta($post->ID,'tasks_date_meta'); ?>" />
	</div>
	<div class="option-item">
		<label>Start time</label>
		<input type="text" name="tasks_start_meta" value="<?php echo hr_meta($post->ID,'tasks_start_meta'); ?>" />
	</div>
	<div class="option-item">
		<label>End time</label>
		<input type="text" name="tasks_end_meta" value="<?php echo hr_meta($post->ID,'tasks_end_meta'); ?>" />
	</div>
	<div class="option-item">
		<label>Service</label>
		<select name="tasks_service_meta">
			<?php
			$arrs = get_posts(array('post_type'=>'services','posts_per_page'=>-1,'orderby'=>'menu_order','order'=>'ASC'));
			foreach($arrs as $arr){
				$selected = (hr_meta($post->ID,'tasks_service_meta')==$arr->ID)?'selected="selected"':'';
				?>
				<option value="<?php echo $arr->ID; ?>" <?php echo $selected; ?>><?php echo $arr->post_title; ?></option>
				<?php
			}
			?>
		</select>
	</div>
	<div class="option-item">
		<label>Name</label>
		<input type="text" name="tasks_name_meta" value="<?php echo hr_meta($post->ID,'tasks_name_meta'); ?>" />
	</div>
	<div class="option-item">
		<label>Price</label>
		<input type="text" name="tasks_price_meta" value="<?php echo hr_meta($post->ID,'tasks_price_meta'); ?>" />
	</div>
	<div class="clearboth"></div>
</div>
<?php	
}

/** SAVE POSTYPE **/	
add_action('save_post', 'save_tasks_meta');
function save_tasks_meta($post_id) {
	global $custom_meta_fields;
	// verify nonce  
	if(isset($_POST['tasks_meta_nonce'])){
		if (!wp_verify_nonce($_POST['tasks_meta_nonce'], basename(__FILE__))){
			return $post_id;
		}
		$metas=array('tasks_date_meta','tasks_start_meta','tasks_end_meta','tasks_name_meta','tasks_service_meta','tasks_price_meta');
		foreach($metas as $meta){
			update_post_meta($post_id, $meta, $_POST[$meta]);
		}
	}
}

/** ADD COLUMN POSTYPE **/

/*---noi bat---*/
add_filter('manage_tasks_posts_columns', 'add_colum_tasks');
function add_colum_tasks($defaults) {
	unset($defaults['date']);
	unset($defaults['author']);
	$defaults['tasks_date_meta'] = 'Date';
	$defaults['tasks_start_meta'] = 'Start';
	$defaults['tasks_end_meta'] = 'End';
	$defaults['tasks_duration_meta'] = 'Duration';
	$defaults['tasks_price_meta'] = 'Price';
	$defaults['tasks_service_meta'] = 'Service';
	$defaults['author'] = 'Author';
	$defaults['date'] = 'Date';
	return $defaults;
}
add_action('manage_tasks_posts_custom_column', 'add_content_colum_tasks', 10, 2);
function add_content_colum_tasks($column_name, $postid) {
	
	switch ($column_name) {
		case 'tasks_date_meta':
		case 'tasks_start_meta':
		case 'tasks_end_meta':
		case 'tasks_price_meta':
			echo hr_meta($postid,$column_name);
			break;
		case 'tasks_duration_meta':
			$duration = strtotime(hr_meta($postid,'tasks_end_meta')) - strtotime(hr_meta($postid,'tasks_start_meta'));
			$hours = ($duration>=3600)?floor($duration / 3600).'h':'';
			$minutes = ($duration%3600 > 0)?floor(($duration / 60) % 60).'m':'';
			echo $hours.$minutes;
			break;
		case 'tasks_service_meta':
			$id_service = hr_meta($postid,$column_name);
			echo ($id_service)?get_post($id_service)->post_title:'';
			break;
	}
}
?>