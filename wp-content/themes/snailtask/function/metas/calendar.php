<?php
/*---- add show home--*/
add_action('add_meta_boxes', 'add_calendar_meta');
function add_calendar_meta() {
	add_meta_box('Option', 'Option', 'show_calendar_meta', 'calendar');
}
function show_calendar_meta() {
	global $post;
	echo '<input type="hidden" name="calendar_meta_nonce" value= "' . wp_create_nonce(basename(__FILE__)) . '"/>';
?>
<div class="option option-calendar">
	<div class="option-item">
		<label>Start</label>
		<input type="text" name="calendar_date_meta" value="<?php echo hr_meta($post->ID,'calendar_date_meta'); ?>" />
	</div>
	<div class="option-item">
		<label>End</label>
		<input type="text" name="calendar_end_meta" value="<?php echo hr_meta($post->ID,'calendar_end_meta'); ?>" />
	</div>
	<div class="option-item">
		<label>All Day</label>
		<input type="text" name="calendar_allday_meta" value="<?php echo hr_meta($post->ID,'calendar_allday_meta'); ?>" />
	</div>
	
	<div class="option-item">
		<label>Customer</label>
		<textarea name="calendar_customer_meta"><?php echo hr_meta($post->ID,'calendar_customer_meta'); ?></textarea>
	</div>
	<div class="option-item">
		<label>Description</label>
		<textarea name="calendar_desrc_meta"><?php echo hr_meta($post->ID,'calendar_desrc_meta'); ?></textarea>
	</div>
	<div class="clearboth"></div>
</div>
<?php	
}

/** SAVE POSTYPE **/	
add_action('save_post', 'save_calendar_meta');
function save_calendar_meta($post_id) {
	global $custom_meta_fields;
	// verify nonce  
	if(isset($_POST['calendar_meta_nonce'])){
		if (!wp_verify_nonce($_POST['calendar_meta_nonce'], basename(__FILE__))){
			return $post_id;
		}
		$metas=array('calendar_date_meta','calendar_end_meta','calendar_allday_meta','calendar_customer_meta','calendar_desrc_meta');
		foreach($metas as $meta){
			update_post_meta($post_id, $meta, $_POST[$meta]);
		}
	}
}

/** ADD COLUMN POSTYPE **/

/*---noi bat---*/
add_filter('manage_calendar_posts_columns', 'add_colum_calendar');
function add_colum_calendar($defaults) {
	unset($defaults['date']);
	unset($defaults['author']);
	$defaults['calendar_date_meta'] = 'Date';
	$defaults['calendar_end_meta'] = 'End';
	$defaults['calendar_allday_meta'] = 'All Day';
	$defaults['calendar_customer_meta'] = 'Customer';
	$defaults['author'] = 'Author';
	$defaults['date'] = 'Date';
	return $defaults;
}
add_action('manage_calendar_posts_custom_column', 'add_content_colum_calendar', 10, 2);
function add_content_colum_calendar($column_name, $postid) {
	switch ($column_name) {
		case 'calendar_date_meta':
		case 'calendar_end_meta':
		case 'calendar_allday_meta':
		case 'calendar_customer_meta':
			echo hr_meta($postid,$column_name);
			break;
	}
}
?>