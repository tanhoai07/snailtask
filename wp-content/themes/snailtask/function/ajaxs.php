<?php
add_action('wp_ajax_login','login');
add_action('wp_ajax_nopriv_login','login');
function login(){
	$err=0;$params=array();
	extract($_POST);
	if(!username_exists($log_email)){
		$err=1; $params[]='log_email';
		add_notice(__( "Tài khoản này không tồn tại.", 'snailtask' ),0,'.login_submit');
		exit;
	}
	if(!$err){
		$remember = ($log_remember==1)?true:false;
		$result = login_within_pass($log_email,$log_password,$remember);
		if(is_wp_error($result)){
			$err=1; $params[]='log_password';
			update_json_user($user->ID,'wrong_password_meta',$log_password);
			add_notice(__( "Mật khẩu chưa đúng.", 'snailtask' ),0,'.login_submit');
			exit;
		}
		else{
			$caps = get_user_by('login',$log_email);
			$link = get_permalink(get_page_by_path('task'));
			if($caps->roles[0]=='administrator'){
				$link = get_permalink(get_page_by_path('employees'));
			}
			if($caps->roles[0]=='editor'){
				$link = home_url('/');
			}			
			add_notice(__( 'Đăng nhập thành công.', 'snailtask'),1);
			?>
			<script>
				window.location='<?php echo $link; ?>';
			</script>
			<?php
		}
	}
	?>
	<script>
		jQuery(document).ready(function(){
			<?php
				$arrs=array('log_email','log_password');
				foreach($arrs as $arr){
					if(in_array($arr,$params)){
						?>jQuery("#<?php echo $arr ?>").addClass('error');<?php
					}
					else{
						?>jQuery("#<?php echo $arr ?>").removeClass('error');<?php
					}
				}
			?>
			unloading(".login_submit");
		})
	</script>
	<?php
	exit;
}

/** add task ajax **/
add_action('wp_ajax_add_task','add_task');
add_action('wp_ajax_nopriv_add_task','add_task');
function add_task(){
	$err=0;$params=array();
	extract($_POST);
	
	$arrs = array('addt_start','addt_end','addt_name','addt_price');
	foreach($arrs as $arr){
		if($_POST[$arr]==""){
			$err=1; $params[]=$arr;
		}
	}
	
	if(strtotime($addt_end)<=strtotime($addt_start)){
		$err=1; $params[]='addt_start'; $params[]='addt_end ';
	}
		
	if(!is_numeric($addt_price)){
		$err=1; $params[]='addt_price';
	}	
	
	if(!$err){
		$my_post = array(
			 'post_type' => 'tasks',
			 'post_title' => $addt_name,
			 'post_status' => 'publish',
			 'post_author' => MYID,
		);
		$new_tasks = wp_insert_post($my_post);
		if($new_tasks){
			$metas=array(
				'tasks_date_meta'=>date('Y-m-d',current_time('timestamp')),
				'tasks_start_meta'=>$addt_start,
				'tasks_end_meta'=>$addt_end,
				'tasks_name_meta'=>$addt_name,
				'tasks_service_meta'=>$addt_service,
				'tasks_price_meta'=>$addt_price,
			);
			foreach($metas as $key=>$value){
				update_post_meta($new_tasks,$key,$value);
			}
			add_notice(__( 'Thêm task thành công.', 'snailtask'),1);
			?>
			<script>
				window.location='<?php echo get_permalink(get_page_by_path('accounting')); ?>';
			</script>
			<?php
		}
		else{
			add_notice(__( "Lỗi hệ thống.", 'snailtask' ),0,'.addt_submit');
			exit;
		}
	}
	else{
		add_notice(__( "Dữ liệu chưa đúng.", 'snailtask' ),0);
	}
	?>
	<script>
		jQuery(document).ready(function(){
			<?php
				$arrs=array('addt_start','addt_end','addt_name','addt_price');
				foreach($arrs as $arr){
					if(in_array($arr,$params)){
						?>jQuery("#<?php echo $arr ?>").addClass('error');<?php
					}
					else{
						?>jQuery("#<?php echo $arr ?>").removeClass('error');<?php
					}
				}
			?>
			unloading(".addt_submit");
		})
	</script>
	<?php
	exit;
}

/** add task ajax **/
add_action('wp_ajax_load_employee','load_employee');
add_action('wp_ajax_nopriv_load_employee','load_employee');
function load_employee(){
	$number=9;
	$args = array('number'=>$number,'role'=>'author','offset'=>($_POST['pg']-1)*$number);
	$arrs = get_users($args);
	foreach($arrs as $num=>$arr){
		require get_template_directory() . '/parts/employee.php';
	}
	?>
	<script>unloading('.click_list_employee[data-page="<?php echo $_POST['pg']; ?>"]')</script>
	<?php
exit;
}

/** add task ajax **/
add_action('wp_ajax_search_employee','search_employee');
add_action('wp_ajax_nopriv_search_employee','search_employee');
function search_employee(){
	$arrs = get_users(array('number'=>-1,'role'=>'author'));
	foreach($arrs as $arr){
		$content = ' '.$arr->display_name.' '.$arr->user_email;
		$metas=array('user_address_meta','user_phone_meta','user_salary_meta','user_studio_meta');
		foreach($metas as $meta){
			update_user_meta($user_id, $meta, $_POST[$meta]);
			$content = $content.' '.get_the_author_meta($meta,$arr->ID);
		}
		if(!(strpos(strtolower($content),strtolower($_POST['key'])) === false)){
			require get_template_directory() . '/parts/employee.php';
		}
	}
	?>
	<script>unloading('.submit_search_employee')</script>
	<?php
exit;
}

add_action('wp_ajax_xl_upload','xl_upload');
add_action('wp_ajax_nopriv_xl_upload','xl_upload');
function xl_upload(){
	 $err=0;$data=$thumb_id='';
	 $max_filesize = 10097152;
	 $allowed_filetypes = array('.jpg','.jpeg','.gif','.png','.JPG','.PNG','.JPEG','.GIF');
	 $upload_dir= wp_upload_dir();
	 $upload_path =UPLOAD_DIR.'/temp/';
	 $upload_url=UPLOAD_URI.'/temp/';
	 if(!is_writable($upload_path)) {
		  $err=1;
		  $data = '<div class="error">Thư mục upload ko thể ghi</div>';
	 }
	 $filenames = $_FILES['userfile']['name'];
	 foreach($filenames as $filename){
	 	$ext = substr($filename, strpos($filename,'.'), strlen($filename)-1); 
	 	if(!in_array($ext,$allowed_filetypes)) {
				$err=1;
				$data = '<div class="error">Only(jpg / png / gif) file is supported.</div>';
		}
	 }
	 $filetemps=$_FILES['userfile']['tmp_name'];
	 foreach($filetemps as $filetemp){
	 	 if(filesize($filetemp) > $max_filesize) {
			  $err=1;
			  $data = '<div class="error">File upload phải nhỏ hơn 10M.</div>';
		 }
	 }
	 if(!$err){
		 $count=0;
		 foreach($filetemps as $filetemp){
			$file_strip = str_replace(" ","-",str_replace(".","_".time().".",$filenames[$count])); 
			if(move_uploaded_file($filetemp,$upload_path . $file_strip)) {
				 $data =  $upload_url.$file_strip;
				 $thumb_id = get_attach_thumb_id($data,2);
			} 
			else {
				 $err=1;
				 $data = '<div class="error">'. $file_strip .' error</div>';
			} 
		 $count++;
		 }
	 }
	 echo json_encode(array('error'=>$err,'data'=>$data,'thumb_id'=>$thumb_id));
exit;
}

/*****/
add_action('wp_ajax_update_employee','update_employee');
add_action('wp_ajax_nopriv_update_employee','update_employee');
function update_employee(){
if(!is_hr_admin() && MYID <> $_POST['user_id_meta']){
	add_notice(__( "Bạn không có quyền này", 'snailtask' ),0, '.btn_submit_profile');
	exit;
}
$uid = (is_hr_admin() && isset($_POST['user_id_meta']) && $_POST['user_id_meta'])?$_POST['user_id_meta']:MYID;
$user_id = wp_update_user( array( 'ID' => $uid, 'display_name' => $_POST['user_name_meta']) );
$arrs = array('st_user_avatar','user_address_meta','user_phone_meta','user_salary_meta','user_studio_meta');
foreach($arrs as $arr){
	update_user_meta($uid,$arr,$_POST[$arr]);
}
if($_POST['user_password_meta']){
	wp_set_password($_POST['user_password_meta'], $uid );
}
?>
<script>
	add_notice('<font color="green">Cập nhật thành công.</font>');
	unloading('.btn_submit_profile');
</script>
<?php
exit;
}

/*****/
add_action('wp_ajax_update_task','update_task');
add_action('wp_ajax_nopriv_update_task','update_task');
function update_task(){
	$err=0;$params=array();
	extract($_POST);
	$tid = $addt_id;
	$task=get_post($tid);
	if(!(is_hr_admin('administrator') || $task->post_author == MYID)){
		add_notice(__( "Bạn không có quyền này", 'snailtask' ),0,'.updatet_submit');
		exit;
	}
	
	$arrs = array('addt_start','addt_end','addt_name','addt_price');
	foreach($arrs as $arr){
		if(trim($_POST[$arr]) == ""){
			$err=1; $params[]=$arr;
		}
	}
	
	if(strtotime($addt_end)<=strtotime($addt_start)){
		$err=1; $params[]='addt_start'; $params[]='addt_end ';
	}
		
	if(!is_numeric($addt_price)){
		$err=1; $params[]='addt_price';
	}
	if(!$err){
		$metas=array(
			'tasks_start_meta'=>$addt_start,
			'tasks_end_meta'=>$addt_end,
			'tasks_name_meta'=>$addt_name,
			'tasks_service_meta'=>$addt_service,
			'tasks_price_meta'=>$addt_price,
		);
		foreach($metas as $key=>$value){
			update_post_meta($tid,$key,$value);
		}
		add_notice(__( 'Cập nhật thành công.', 'snailtask'),1);
	}else{
		add_notice(__( "Dữ liệu chưa đúng.", 'snailtask' ),0);
	}
	?>
	<script>
		jQuery(document).ready(function(){
			<?php
				$arrs=array('addt_start','addt_end','addt_name','addt_price');
				foreach($arrs as $arr){
					if(in_array($arr,$params)){
						?>jQuery("#<?php echo $arr ?>").addClass('error');<?php
					}
					else{
						?>jQuery("#<?php echo $arr ?>").removeClass('error');<?php
					}
				}
			?>
			unloading(".updatet_submit");
		})
	</script>
	<?php
exit;
}

/*****/
add_action('wp_ajax_get_accounting_list','get_accounting_list');
add_action('wp_ajax_nopriv_get_accounting_list','get_accounting_list');
function get_accounting_list(){
	$arrs = get_employee_by_date($_POST['my_date_accounting']);
	require_once get_template_directory() . '/parts/accounting.php';
	?>
	<script>
		unloading('.list_accounting_loading');
	</script>
	<?php
exit;
}

/*****/
add_action('wp_ajax_click_paid','click_paid');
add_action('wp_ajax_nopriv_click_paid','click_paid');
function click_paid(){
	if(!is_hr_admin()){
		add_notice('Bạn không có quyền này.', 0, '.click_paid[data-id="'.$_POST['ids'].'"]');
		exit;
	}
	extract($_POST);
	$ids = explode(',',$ids);
	$paid = ($paid)?0:1;
	foreach($ids as $id){
		update_post_meta($id,'hr_paid',$paid);
	}
	ob_start();
	?>
	<a href="javascript:void(0)" data-id="<?php echo $_POST['ids']; ?>" data-value="<?php echo $paid; ?>" class="<?php echo (is_hr_admin())?'click_paid':'';?> click_paid_css">
		<i class="fa <?php echo $paid?'fa-check-circle paid':'fa-money unpay'; ?>"></i>
	</a>
	<?php
	$content = ob_get_clean();
	?>
	<textarea class="ajax_content"><?php echo $content; ?></textarea>
	<script>
		jQuery('.click_paid[data-id="<?php echo $_POST['ids'] ?>"]').parent('td').html(jQuery('.ajax_content').val());
	</script>
	<?php
exit;
}


/*****/
add_action('wp_ajax_add_service','add_service');
add_action('wp_ajax_nopriv_add_service','add_service');
function add_service(){
	$err=0;$params=array();
	extract($_POST);
	if(!(is_hr_admin())){
		add_notice(__( "Bạn không có quyền này", 'snailtask' ),0,'.add_service_button');
		exit;
	}
	$arrs = array('services_title_meta','services_duration_meta','services_price_meta','services_studio_meta');
	foreach($arrs as $arr){
		if($_POST[$arr] == ""){
			$err=1; $params[]=$arr;
		}
	}
	
	if(!$err){
		$my_post = array(
			 'post_type' => 'services',
			 'post_title' => $_POST['services_title_meta'],
			 'post_status' => 'publish',
		);
		$new_services = wp_insert_post($my_post);
		if($new_services){
			$metas=array('services_duration_meta','services_price_meta','services_studio_meta');
			foreach($metas as $meta){
				update_post_meta($new_services,$meta,$_POST[$meta]);
			}
			add_notice(__( 'Thêm dịch vụ thành công.', 'snailtask'),1);
			ob_start();
			?>
			<tr data-id="<?php echo $new_services; ?>">
				<td><input type="text" class="edittext update_service services_title_meta" value="<?php echo $_POST['services_title_meta']; ?>"></td>
				<td><input type="text" class="edittext update_service services_duration_meta" value="<?php echo $_POST['services_duration_meta'];?>"></td>
				<td><input type="text" class="edittext update_service services_price_meta" value="<?php echo $_POST['services_price_meta'];?>"></td>
				<td><input type="text" class="edittext update_service services_studio_meta" value="<?php echo $_POST['services_studio_meta'];?>"></td>
				<td class="align-center"><a href="javascript:void(0);" class="delete_service"><i class="fa fa-trash"></i></a></td>
			</tr>
			<?php
			$content = ob_get_clean();
			?>
			<textarea class="ajax_content"><?php echo $content; ?></textarea>
			<script>
				jQuery('.table-add-service').find('tbody').prepend(jQuery('.ajax_content').val());
				unloading('.add_service_button');
				jQuery('.add_service_model').modal('hide');
				jQuery('.add_service_form')[0].reset();
			</script>
			<?php
		}		
	}else{
		add_notice(__( "Bạn phải nhập tất cả thông tin.", 'snailtask' ),0);
	}
	?>
	<script>
		jQuery(document).ready(function(){
			<?php
				$arrs=array('services_title_meta','services_duration_meta','services_price_meta','services_studio_meta');
				foreach($arrs as $arr){
					if(in_array($arr,$params)){
						?>jQuery("#<?php echo $arr ?>").addClass('error');<?php
					}
					else{
						?>jQuery("#<?php echo $arr ?>").removeClass('error');<?php
					}
				}
			?>
			unloading('.add_service_button');
		})
	</script>
	<?php
exit;
}

add_action('wp_ajax_delete_service','delete_service');
add_action('wp_ajax_nopriv_delete_service','delete_service');
function delete_service(){
	$err=0;$params=array();
	extract($_POST);
	if(!(is_hr_admin())){
		add_notice(__( "Bạn không có quyền này", 'snailtask' ),0, 'tr[data-id="'.$id.'"] .delete_service');
		exit;
	}
	if(wp_delete_post($id)){
		add_notice(__( 'Xóa dịch vụ thành công.', 'snailtask'),1);
		?>
		<script>
			jQuery('tr[data-id="<?php echo $id; ?>"]').fadeOut();
		</script>
		<?php
	}
	exit;
}


add_action('wp_ajax_update_service','update_service');
add_action('wp_ajax_nopriv_update_service','update_service');
function update_service(){
	$err=0;$params=array();
	extract($_POST);
	if(!(is_hr_admin())){
		add_notice(__( "Bạn không có quyền này", 'snailtask' ),0);
		exit;
	}
	$arrs = array('services_title_meta','services_duration_meta','services_price_meta','services_studio_meta');
	foreach($arrs as $arr){
		if($_POST[$arr]==""){
			$err=1; $params[]=$arr;
		}
	}
	if(!$err){
		$my_post = array(
			  'ID'           => $id,
			  'post_title'   => $services_title_meta,
		 );
		$postid = wp_update_post( $my_post );
		if($postid){
			$metas=array('services_duration_meta','services_price_meta','services_studio_meta');
			foreach($metas as $meta){
				update_post_meta($postid,$meta,$_POST[$meta]);
			}
			add_notice(__( 'Cập nhật dịch vụ thành công.', 'snailtask'),1);
		}		
	}else{
		add_notice(__( "Dịch vụ chưa cập nhật được do thiếu thông tin", 'snailtask' ),0);
	}
	exit;
}


add_action('wp_ajax_delete_user','delete_user');
add_action('wp_ajax_nopriv_delete_user','delete_user');
function delete_user(){
	$err=0;$params=array();
	extract($_POST);
	if(!(is_hr_admin())){
		add_notice(__( "Bạn không có quyền này", 'snailtask' ),0, '.delete_user[data-id="'.$id.'"]');
		exit;
	}
	if(wp_delete_user($id)){
		add_notice(__( 'Xóa nhân viên thành công.', 'snailtask'),1);
		?>
		<script>
			jQuery('.delete_user[data-id="<?php echo $id; ?>"]').parents('.profile_details').fadeOut();
		</script>
		<?php
	}
	exit;
}



/**** add employee ***/
/*****/
add_action('wp_ajax_add_employee','add_employee');
add_action('wp_ajax_nopriv_add_employee','add_employee');
function add_employee(){
	$err=0;$params=array();
	extract($_POST);
	if(!(is_hr_admin())){
		add_notice(__( "Bạn không có quyền này", 'snailtask' ),0,'.add_employee_button');
		exit;
	}
	$arrs = array('user_name_meta','user_password_meta','user_fullname_meta','user_phone_meta','user_address_meta','user_salary_meta','user_studio_meta');
	foreach($arrs as $arr){
		if($_POST[$arr]==""){
			$err=1; $params[]=$arr;
		}
	}
	
	if(!$err){
		$userdata = array(
			'user_login'  => $user_name_meta,
			'user_pass'   => $user_password_meta,
			'user_email'  => 'snailtask_'.$user_phone_meta.'@gmail.com',
			'first_name'  => $user_fullname_meta,
			'role'		  => 'author'
		);
		$user_id = wp_insert_user( $userdata ) ;		
		if (!is_wp_error($user_id)){
			$arrs = array('user_phone_meta','user_address_meta','user_salary_meta','user_studio_meta');
			foreach($arrs as $arr){
				update_user_meta($user_id,$arr,$_POST[$arr]);
			}
			add_notice(__( "Thêm nhân viên thành công", 'snailtask' ),1);
			ob_start();
			$arr = get_user_by('id',$user_id);
			require get_template_directory() . '/parts/employee.php';
			$content = ob_get_clean();
			?>
			<textarea class="ajax_content"><?php echo $content; ?></textarea>
			<script>
				if(jQuery('.profile_details').eq(8).length){
					jQuery('.profile_details').eq(8).remove();
				}
				jQuery('#list_employee').prepend(jQuery('.ajax_content').val());
				unloading('.add_employee_button');
				jQuery('.add_employee_model').modal('hide');
				jQuery('.add_employee_form')[0].reset();
			</script>
			<?php
		}else{
			add_notice(__( "Lỗi hệ thống, vui lòng thử lại.", 'snailtask' ),0);
		}	
	}else{
		add_notice(__( "Bạn phải nhập tất cả thông tin.", 'snailtask' ),0);
	}
	?>
	<script>
		jQuery(document).ready(function(){
			<?php
				$arrs=array('user_name_meta','user_password_meta','user_fullname_meta','user_phone_meta','user_address_meta','user_salary_meta','user_studio_meta');
				foreach($arrs as $arr){
					if(in_array($arr,$params)){
						?>jQuery("#<?php echo $arr ?>").addClass('error');<?php
					}
					else{
						?>jQuery("#<?php echo $arr ?>").removeClass('error');<?php
					}
				}
			?>
			unloading('.add_employee_button');
		})
	</script>
	<?php
exit;
}


/*** add claneder **/
add_action('wp_ajax_add_calendar','add_calendar');
add_action('wp_ajax_nopriv_add_calendar','add_calendar');
function add_calendar(){
	$err=0;$params=array();
	extract($_POST);
	if(!(is_hr_admin())){
		add_notice(__( "Bạn không có quyền này", 'snailtask' ),0);
		exit;
	}
	$my_post = array(
	  'post_type'=>'calendar',
	  'post_title'    => $title,
	  'post_status'   => 'publish',
	);	 
	$postid = wp_insert_post( $my_post );
	if($postid){
		update_post_meta($postid,'calendar_date_meta',$start);
		update_post_meta($postid,'calendar_end_meta',$end);
		update_post_meta($postid,'calendar_desrc_meta',$desrc);
		update_post_meta($postid,'calendar_customer_meta',$customer);
		?>
		<script>
			jQuery('#calendar').fullCalendar('renderEvent', 
				{
					id: <?php echo $postid; ?>,
					title: '<?php echo $title; ?>',
					customer: '<?php echo $customer; ?>',
					descr: '<?php echo $descr; ?>',
					start: '<?php echo $start; ?>',
					end: '<?php echo $end; ?>',
				}
			,true);
		</script>
		<?php
	}
	else{
		add_notice(__( "Lỗi hệ thống", 'snailtask' ),0);
	}
	exit;
}

add_action('wp_ajax_update_calendar','update_calendar');
add_action('wp_ajax_nopriv_update_calendar','update_calendar');
function update_calendar(){
	$err=0;$params=array();
	extract($_POST);
	if(!(is_hr_admin())){
		add_notice(__( "Bạn không có quyền này", 'snailtask' ),0);
		exit;
	}
	
	$my_post = array(
		  'ID'           => $id,
		  'post_title'   => $title,
	  );	 
	$postid = wp_update_post( $my_post );
	if($postid){
		update_post_meta($id,'calendar_desrc_meta',$descr);
		update_post_meta($id,'calendar_customer_meta',$customer);
	}
	if($title==""){
		?>
		<script>
			window.location=window.location;
		</script>
		<?php
	}	  
	exit;
}
?>