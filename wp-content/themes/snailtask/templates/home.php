<?php
/** Template Name: Home Page */
get_header();

$thisweek=get_current_week();
$lastweek=get_current_week(-1);
$thismonth=get_current_month();
$lastmonth=get_current_month(-1);
$thisyear=get_current_year();
$lastyear=get_current_year(-1);
?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="row tile_count">
        <div class="col-md-3 col-sm-3 col-xs-6 tile_stats_count">
			<span class="count_top"><i class="fa fa-user"></i> Thu nhập ngày</span>
            <div class="count"><?php echo get_amounts(DAY); ?></div>
            <span class="count_bottom"><?php echo the_percent_abs(get_amounts(date('Y-m-d',NOW-3600*24*1)),get_amounts(DAY),' so với hôm qua'); ?></span>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-6 tile_stats_count">
			<span class="count_top"><i class="fa fa-clock-o"></i> Thu nhập tuần</span>
            <div class="count"><?php echo get_amounts($thisweek[0],$thisweek[1])?></div>
            <span class="count_bottom"><?php echo the_percent_abs(get_amounts($lastweek[0],$lastweek[1]),get_amounts($thisweek[0],$thisweek[1]),' so với tuần trước'); ?></span>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-user"></i> Thu nhập tháng</span>
            <div class="count green"><?php echo get_amounts($thismonth[0],$thismonth[1]); ?></div>
            <span class="count_bottom"><?php echo the_percent_abs(get_amounts($lastmonth[0],$lastmonth[1]),get_amounts($thismonth[0],$thismonth[1]),' so với tháng trước'); ?></span>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-6 tile_stats_count">
			<span class="count_top"><i class="fa fa-user"></i> Thu nhập năm</span>
            <div class="count"><?php echo get_amounts($thisyear[0],$thisyear[1])?></div>
            <span class="count_bottom"><?php echo the_percent_abs(get_amounts($lastyear[0],$lastyear[1]),get_amounts($thisyear[0],$thisyear[1]),' so với năm trước'); ?></span>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Sơ đồ thu nhập tháng</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="demo-container" style="height:280px">
							<div id="chart_plot_02" class="demo-placeholder"></div>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="x_panel fixed_height_320">
                <div class="x_title">
                    <h2>Nhân viên xuất sắc ngày</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
				<?php
				$arrs = get_top_employee();
				if(!empty($arrs)){
					$amount_total = get_amounts(DAY);
					foreach($arrs as $employeeid => $amount){
						$percent = round($amount*100/$amount_total,2).'%';
						?>
						<div class="widget_summary">
							<div class="w_left w_25">
								<span><?php echo get_name_employee($employeeid); ?></span>
							</div>
							<div class="w_center w_55">
								<div class="progress">
									<div class="progress-bar bg-green" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $percent; ?>;">
										<span class="sr-only"><?php echo $percent; ?> Complete</span>
									</div>
								</div>
							</div>
							<div class="w_right w_20">
								<span><?php echo $percent; ?></span>
							</div>
							<div class="clearfix"></div>
						</div>
						<?php
					}
				}
				?>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="x_panel tile fixed_height_320 overflow_hidden">
                <div class="x_title">
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table class="" style="width:100%">
                        <tbody><tr>
                            <th style="width:37%;">
                                <p>Top 5</p>
                            </th>
                            <th>
                                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                                    <p class="">Dịch vụ</p>
                                </div>
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                    <p class="">Phần trăm</p>
                                </div>
                            </th>
                        </tr>
                        <tr>
                            <td>
                                <canvas class="canvasDoughnut" height="140" width="140" style="margin: 15px 10px 10px 0px; width: 140px; height: 140px;"></canvas>
                            </td>
                            <td>
                                <table class="tile_info">
                                    <tbody>
									<?php
									$arrs = get_top_service();
									if(!empty($arrs)){
										$amount_total = get_amounts(DAY);
										$color = get_color();
										$count=0;
										foreach($arrs as $serviceid => $amount){
											$count++;
											$percent = round($amount*100/$amount_total,2).'%';
											if($count>5){ break; }
											?>
											<tr>
												<td>
													<p><i class="fa fa-square <?php echo $color[$count-1]; ?>"></i><?php echo get_the_title($serviceid); ?></p>
												</td>
												<td><?php echo $percent; ?></td>
											</tr>
											<?php
										}
									}
									?>									
                                    </tbody>
								</table>
                            </td>
                        </tr>
                        </tbody></table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->
<?php get_footer(); ?>
