<?php
/** Template Name: Login */
get_header('login');
?>
<div>
    <a class="hiddenanchor" id="signup"></a>
    <a class="hiddenanchor" id="signin"></a>
    <div class="login_wrapper">
        <div class="animate form login_form">
            <img src="<?php echo THEME_URI ?>/images/logo.png" alt="logo" width="150">
            <section class="login_content">
                <form onsubmit="return false" class="login_form_submit">
                    <h1>Đăng nhập</h1>
                    <div>
                        <input type="text" class="form-control" placeholder="Username" required="" id="log_email">
                    </div>
                    <div>
                        <input type="password" class="form-control" placeholder="Password" required="" id="log_password">
                    </div>
					<input type="hidden" value="<?php echo wp_create_nonce($_SESSION['login_nonce'] = rand(11111,99999)); ?>" id="login_nonce" />
					<div>
						<button class="btn btn-info btn-lg submit login_submit">Đăng nhập</button>
                    </div>
                    <div class="clearfix"></div>
                </form>
            </section>
        </div>

        <div id="register" class="animate form registration_form">
            <section class="login_content">
                <form>
                    <h1>Create Account</h1>
                    <div>
                        <input type="text" class="form-control" placeholder="Username" required="">
                    </div>
                    <div>
                        <input type="email" class="form-control" placeholder="Email" required="">
                    </div>
                    <div>
                        <input type="password" class="form-control" placeholder="Password" required="">
                    </div>
                    <div>
                        <a class="btn btn-default submit" href="index.html">Submit</a>
                    </div>

                    <div class="clearfix"></div>

                    <div class="separator">
                        <p class="change_link">Already a member ?
                            <a href="#signin" class="to_register"> Log in </a>
                        </p>

                        <div class="clearfix"></div>
                        <br>

                        <div>
                            <h1><i class="fa fa-paw"></i> NailsMedia</h1>

                        </div>
                    </div>
                </form>
            </section>
        </div>
    </div>
</div>
<?php get_footer('login'); ?>
