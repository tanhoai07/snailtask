<?php
/** Template Name: History */
get_header();
$uid = (current_user_can('administrator') && isset($_GET['uid']) && $_GET['uid'])?$_GET['uid']:MYID;
?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Lịch sử</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>

            <div class="x_content">
                <div class="table-responsive">
                    <table class="table table-striped jambo_table bulk_action">
                        <thead>
							<tr class="headings">
								<th class="column-title">Stt </th>
								<th class="column-title">Ngày </th>
								<th class="column-title">Bắt đầu </th>
								<th class="column-title">Kết thúc</th>
								<th class="column-title">Khách hàng </th>
								<th class="column-title">Số tiền </th>
								<th class="column-title">Dịch vụ </th>
								<th class="column-title no-link last align-center"><span class="nobr"></span></th>
							</tr>
                        </thead>

                        <tbody>
							<?php
							$args = array(
								'post_type'=>'tasks',
								'posts_per_page'=>-1,
								'author'=>$uid,
								'orderby'=>'meta_value',
								'meta_key'=>'tasks_date_meta',
								'order'=>'ASC',
							);
							$arrs = get_posts($args);
							foreach($arrs as $num=>$arr){
								?>
								<tr class="even pointer">
									<td data-title="Stt"><?php echo $num; ?></td>
									<?php
									$metas = array('tasks_date_meta','tasks_start_meta','tasks_end_meta','tasks_name_meta','tasks_price_meta');

									foreach($metas as $key => $meta){
                                       if($key==0){
                                           $text ='Ngày';
                                       }elseif($key==1){
                                           $text ='Bắt đầu';
                                       }elseif($key==2){
                                           $text ='Kết thúc';
                                       }elseif($key==3){
                                           $text ='Khách hàng';
                                       }elseif($key==4){
                                           $text ='Số tiền';
                                       }
									?>
									<td data-title="<?php echo $text;?>"><?php echo hr_meta($arr->ID,$meta); ?></td>
									<?php
									}
									?>
									<td data-title="Dịch vụ"><?php echo get_the_title(hr_meta($arr->ID,'tasks_service_meta')); ?></td>
									<td class="align-center" data-title="Sửa"><a href="<?php echo get_permalink(get_page_by_path('task')).'?tid='.$arr->ID ?>">Sửa</a></td>
								</tr>
								<?php
							}
							?>
                        </tbody>
                    </table>
                </div>


            </div>
        </div>
    </div>
</div>
<!-- /page content -->
<?php get_footer(); ?>
