<?php
/** Template Name: Employees */
get_header();
?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Danh sách nhân viên</h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    <form class="form_search_employee" onsubmit="return false">
						<div class="input-group">
							<input type="text" class="form-control" placeholder="Từ khóa ..." id="key_employee">
							<span class="input-group-btn">
							  <button class="btn btn-default submit_search_employee" type="submit">Tìm!</button>
							</span>
						</div>
					</form>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                                <ul class="pagination pagination-split">
                                    <?php
								   $number=9;
								   $args = array('number'=>-1,'role'=>'author');
								   $all = count(get_users($args));
								   $percent = $all % $number;
        						   $number_page = ($percent)?(($all-$percent)/$number + 1):($all/$number);
								   if($number_page>1){
								   		for($i=1; $i<=$number_page; $i++){
											?>
											<li><a href="javascript:void(0)" class="<?php echo ($i==1)?'active':'';?> click_list_employee" data-page="<?php echo $i; ?>"><?php echo $i; ?></a></li>
											<?php
										}
								   }
								   ?>
                                </ul>
                            </div>

                            <div class="clearfix"></div>
							<div id="list_employee" class="<?php echo (!is_hr_admin())?'disallow-delete-user':''; ?>">
							<?php
								$page= isset($_GET['pg'])?$_GET['pg']:1;
								$args['number']=$number;
								$args['offset']=($page-1)*$number;
								$arrs = get_users($args);
								foreach($arrs as $num=>$arr){
									require get_template_directory() . '/parts/employee.php';
								}
								?>
							</div>
							<div class="clearfix"></div>
							
							<!-- Large modal -->
							<div class="wrap-modal-button">
							  <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".add_employee_model">Thêm nhân viên</button>
							 </div>
			
							  <div class="modal fade add_employee_model" tabindex="-1" role="dialog" aria-hidden="true">
								<div class="modal-dialog modal-lg">
								  <div class="modal-content">
									<div class="modal-header">
									  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
									  </button>
									  <h4 class="modal-title" id="myModalLabel">Nhập thông tin dịch vụ</h4>
									</div>
									<div class="modal-body">
										<form onsubmit="return false" class="form-horizontal form-label-left add_employee_form">
											  <div class="form-group">
												<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tên đăng nhập</label>
												<div class="col-md-6 col-sm-6 col-xs-12">
												  <input type="text" id="user_name_meta" class="form-control col-md-7 col-xs-12">
												</div>
											  </div>
											  <div class="form-group">
												<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Mật khẩu</label>
												<div class="col-md-6 col-sm-6 col-xs-12">
												  <input type="password" id="user_password_meta" name="last-name" class="form-control col-md-7 col-xs-12">
												</div>
											  </div>
											  <div class="form-group">
												<label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Họ Tên</label>
												<div class="col-md-6 col-sm-6 col-xs-12">
												  <input id="user_fullname_meta" class="form-control col-md-7 col-xs-12" type="text" name="middle-name">
												</div>
											  </div>
											  <div class="form-group">
												<label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Số điện thoại</label>
												<div class="col-md-6 col-sm-6 col-xs-12">
												  <input id="user_phone_meta" class="form-control col-md-7 col-xs-12" type="text" name="middle-name">
												</div>
											  </div>
											  <div class="form-group">
												<label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Địa chỉ</label>
												<div class="col-md-6 col-sm-6 col-xs-12">
												  <input id="user_address_meta" class="form-control col-md-7 col-xs-12" type="text" name="middle-name">
												</div>
											  </div>
											  <div class="form-group">
												<label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Tỷ lệ ăn chia</label>
												<div class="col-md-6 col-sm-6 col-xs-12">
												  <select class="form-control" id="user_salary_meta">
												  	<?php
													$arrs  = get_share_rating();
													if(!empty($arrs)){
														foreach($arrs as $val => $name){
														?>
														<option value="<?php echo $val; ?>"><?php echo $name; ?></option>
														<?php
														}
													}
													?>
												  </select>
												</div>
											  </div>
											  <div class="form-group">
												<label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Cửa hàng</label>
												<div class="col-md-6 col-sm-6 col-xs-12">
												  <select class="form-control" id="user_studio_meta">
												  	<?php
													$arrs  = get_posts(array('post_type'=>'studios','posts_per_page'=>-1,'orderby'=>'menu_order','order'=>'ASC'));
													if(!empty($arrs)){
														foreach($arrs as $arr){
														?>
														<option value="<?php echo $arr->ID; ?>"><?php echo $arr->post_title; ?></option>
														<?php
														}
													}
													?>
												  </select>
												</div>
											  </div>
										</form>
									</div>
									<div class="modal-footer">
									  <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
									  <button type="button" class="btn btn-primary add_employee_button">Thêm nhân viên</button>
									</div>
			
								  </div>
								</div>
							  </div>
							<!-- Large modal -->
							
							
															 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->
<?php get_footer(); ?>