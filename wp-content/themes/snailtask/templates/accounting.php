<?php
/** Template Name: Accounting */
get_header();
?>
<link href="<?php echo THEME_URI ?>/css/bootstrap-datetimepicker.css" rel="stylesheet">
<!-- page content -->
<div class="right_col" role="main">
	<!--  SECTION LIST NICK -->
	  <div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
			  <div class="x_title">
				<h2>Thu nhập</h2>
				<ul class="nav navbar-right panel_toolbox">
				  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
				  <li><a class="close-link"><i class="fa fa-close"></i></a>
				  </li>
				</ul>
				<div class="clearfix"></div>
			  </div>
			  <div class="x_content">				
				<div class="form-group">
					<div class="label-time">Chọn ngày</div>
					<div class="input-group date myDateAccounting">
						<input type="text" class="form-control" value="<?php echo DAY ?>" id="my_date_accounting">
						<span class="input-group-addon">
						   <span class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>
					<div class="list_accounting_loading"></div>
				</div>
				<div id="list_accounting">
				<?php
					$arrs = get_employee_by_date(DAY);
					require_once get_template_directory() . '/parts/accounting.php';
				?>	
				</div>
			</div>
		  </div>
		</div>
	  </div>
	  <!-- /SECTION LIST NICK -->
</div>


<?php get_footer(); ?>
<!-- bootstrap-datetimepicker -->
<script src="<?php echo THEME_URI ?>/js/bootstrap-datetimepicker.min.js"></script>
<script>
    jQuery('.myDateAccounting').datetimepicker({
        format: 'YYYY-MM-DD'
    }).on('dp.change', function (e) {
		var date = e.date.format(e.date._f);
   		loading('.list_accounting_loading');
		postall_or('list_accounting',hr.a_url+'?action=get_accounting_list','my_date_accounting');
	});
</script>
<!-- /page content -->
