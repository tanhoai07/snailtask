<?php
/** Template Name: User Profile */
get_header();
$uid = (is_hr_admin() && isset($_GET['uid']) && $_GET['uid'])?$_GET['uid']:MYID;
$arr = get_user_by('ID',$uid);
$rates = get_share_rating();
?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Thông tin nhân viên</h3>
            </div>

        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Thông tin</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
                            <div class="profile_img">
                                <div id="crop-avatar">
                                    <!-- Current avatar -->
                                    <label class="hr_wrap_avata" for="myclass">
										<img src="<?php echo get_wp_user_avatar_src($uid, 'medium'); ?>" alt="" class="img-circle img-responsive">
										<input id="userfile" class="input file-input gray-button icon-upload upload" type="file" name="userfile[]" />
										<input type="hidden" id="st_user_avatar" name="st_user_avatar" value="<?php echo get_user_meta($uid,'st_user_avatar',true); ?>" />
										<input type="hidden" id="user_id_meta" name="user_id_meta" value="<?php echo $uid; ?>" />
									</label>
                                </div>
                            </div>
                            <h3><input type="text" class="edittext" value="<?php echo $arr->display_name; ?>" id="user_name_meta" /></h3>
                            <ul class="list-unstyled user_data">
                                <li><i class="fa fa fa-user user-profile-icon"></i> <?php echo $arr->user_login; ?></li>
								<li><i class="fa fa fa-lock user-profile-icon"></i> <input type="password" placeholder="Thay đổi mật khẩu"  class="edittext" value="" id="user_password_meta" /></li><li><hr /></li>
								<?php echo hr_render(get_the_author_meta( 'user_phone_meta', $arr->ID ),'<li><i class="fa fa fa-phone user-profile-icon"></i> <input type="text" class="edittext" value="','" id="user_phone_meta" /></li>');?>
								<li>
									<i class="fa fa-dollar"></i>  Tỉ lệ ăn chia: 
									<select class="edittext edittext-selected <?php echo (!(is_hr_admin())?'disallow-employee':'') ?>" id="user_salary_meta">
										<?php
										$rate_default = get_the_author_meta('user_salary_meta',$arr->ID); 
										foreach($rates as $rvalue=>$rname){
											$selected = ($rvalue==$rate_default)?'selected="selected"':'';
											?>
											<option value="<?php echo $rvalue; ?>" <?php echo $selected; ?>><?php echo $rname; ?></option>
											<?php
										}	
										?>
									</select>
								</li>
								<li>
									<i class="fa fa-globe"></i>  Cửa hàng: 
									<select class="edittext edittext-selected <?php echo (!(is_hr_admin())?'disallow-employee':'') ?>" id="user_studio_meta">
										<?php
										$shop_default = get_the_author_meta('user_studio_meta',$arr->ID); 
										$shops = get_posts(array('post_type'=>'studios','posts_per_page'=>-1,'orderby'=>'menu_order','order'=>'ASC'));
										foreach($shops as $shop){
											$selected = ($shop->ID==$shop_default)?'selected="selected"':'';
											?>
											<option value="<?php echo $shop->ID; ?>" <?php echo $selected; ?>><?php echo $shop->post_title; ?></option>
											<?php
										}	
										?>
									</select>
								</li>
								<?php echo hr_render(get_the_author_meta( 'user_address_meta', $arr->ID ),'<li><i class="fa fa-map-marker user-profile-icon hr_custom_marker"></i> <textarea class="edittext" id="user_address_meta">','</textarea></li>');?>
							</ul>
                            <a class="btn btn-success btn_submit_profile">Cập nhật thông tin</a>
                            <br>

                        </div>
                        <div class="col-md-9 col-sm-9 col-xs-12">

                            <div class="profile_title">
                                <div class="col-md-12">
                                    <h2>Thu nhập trong tháng</h2>
                                </div>
                            </div>
                            <!-- start of user-activity-graph -->
                            <div id="graph_bar" style="width:100%; height:280px;"></div>
                            <!-- end of user-activity-graph -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- /page content -->
<script src="<?php echo THEME_URI; ?>/js/raphael.min.js"></script>
<script src="<?php echo THEME_URI; ?>/js/morris.min.js"></script>
<script src="<?php echo THEME_URI; ?>/js/jquery.ajax.upload.js"></script>
<script>
	var $=jQuery;
	$(document).ready(function() {
		var upload = new AjaxUpload('#userfile', {
				action: '<?php echo admin_url('admin-ajax.php?action=xl_upload') ?>',
				onSubmit : function(file, extension){
				loading('.btn_submit_profile');
				if (! (extension && /^(jpg|png|jpeg|gif)$/.test(extension))){
					 $(".myloading").html('<font color="red">Only（jpg / png / gif）support.</font>');
			   return false;
					} else {
					$('.erroring').hide();
					}	
					upload.setData({'file': file});
				},
				onComplete : function(file, response){
				unloading('.btn_submit_profile');
				var res = JSON.parse(response);
				if(res.error){
					add_notice('<font color="red">'+res.error+'</font>');
				}
				else{
					jQuery(".hr_wrap_avata img").attr('src',res.data);
					jQuery("#st_user_avatar").val(res.thumb_id);
				}
				}
			});
	});	
	
</script>
<?php get_footer(); ?>
