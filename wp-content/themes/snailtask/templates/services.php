<?php
/** Template Name: Services */
get_header();
?>
<!-- page content -->
<div class="right_col" role="main">
	<!--  SECTION LIST NICK -->
	  <div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
			  <div class="x_title">
				<h2>Dịch vụ</h2>
				<ul class="nav navbar-right panel_toolbox">
				  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
				  <li><a class="close-link"><i class="fa fa-close"></i></a>
				  </li>
				</ul>
				<div class="clearfix"></div>
			  </div>
			  <div class="x_content">				
				<div class="list_services">
					<table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap table-add-service <?php echo (!(is_hr_admin())?'disallow-employee':'') ?>" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th>Tên dịch vụ</th>
								<th>Thời lượng</th>
								<th>Giá tiền</th>
								<th>Cửa hàng</th>
								<?php 
								if(is_hr_admin()){
									?>
									<th></th>
									<?php
								}?>
							</tr>
						</thead>
						<tbody class="td-no-padding">
							<?php
								$arrs = get_posts(array('post_type'=>'services','posts_per_page'=>-1));
								if(!empty($arrs)){
									foreach($arrs as $arr){
										?>
										<tr data-id="<?php echo $arr->ID; ?>">
											<td data-title="Tên dịch vụ"><input type="text" class="edittext update_service services_title_meta" value="<?php echo $arr->post_title; ?>"></td>
											<td data-title="Thời lượng"><input type="text" class="edittext update_service services_duration_meta" value="<?php echo hr_meta($arr->ID,'services_duration_meta');?>"></td>
											<td data-title="Giá tiền"><input type="text" class="edittext update_service services_price_meta" value="<?php echo hr_meta($arr->ID,'services_price_meta');?>"></td>
											<td data-title="Cửa hàng"><input type="text" class="edittext update_service services_studio_meta" value="<?php echo hr_meta($arr->ID,'services_studio_meta');?>"></td>
											<?php 
											if(is_hr_admin()){
												?>
												<td class="align-center"><a href="javascript:void(0);" class="delete_service"><i class="fa fa-trash"></i></a></td>
												<?php
											}?>
										</tr>
										<?php
									}
								}
							?>
						</tbody>
					</table>
					<?php
					if(is_hr_admin()){
					?>
					<!-- Large modal -->
					  <div class="wrap-modal-button">
					  	<button type="button" class="btn btn-primary" data-toggle="modal" data-target=".add_service_model">Thêm dịch vụ</button>
					  </div>
					  <div class="modal fade add_service_model" tabindex="-1" role="dialog" aria-hidden="true">
						<div class="modal-dialog modal-lg">
						  <div class="modal-content">
							<div class="modal-header">
							  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
							  </button>
							  <h4 class="modal-title" id="myModalLabel">Nhập thông tin dịch vụ</h4>
							</div>
							<div class="modal-body">
								<form onsubmit="return false" class="form-horizontal form-label-left add_service_form">
									  <div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tên dịch vụ</label>
										<div class="col-md-6 col-sm-6 col-xs-12">
										  <input type="text" id="services_title_meta" class="form-control col-md-7 col-xs-12">
										</div>
									  </div>
									  <div class="form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Thời lượng</label>
										<div class="col-md-6 col-sm-6 col-xs-12">
										  <input type="text" id="services_duration_meta" name="last-name" class="form-control col-md-7 col-xs-12">
										</div>
									  </div>
									  <div class="form-group">
										<label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Giá tiền</label>
										<div class="col-md-6 col-sm-6 col-xs-12">
										  <input id="services_price_meta" class="form-control col-md-7 col-xs-12" type="text" name="middle-name">
										</div>
									  </div>
									  <div class="form-group">
										<label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Cửa hàng</label>
										<div class="col-md-6 col-sm-6 col-xs-12">
										  <input id="services_studio_meta" class="form-control col-md-7 col-xs-12" type="text" name="middle-name">
										</div>
									  </div>
								</form>
							</div>
							<div class="modal-footer">
							  <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
							  <button type="button" class="btn btn-primary add_service_button">Thêm dịch vụ</button>
							</div>
	
						  </div>
						</div>
					  </div>
					<!-- Large modal -->
					<?php
					}
					?>
					
				</div>
			</div>
		  </div>
		</div>
	  </div>
	  <!-- /SECTION LIST NICK -->
</div>
<!-- /page content -->
<?php get_footer(); ?>
