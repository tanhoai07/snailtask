<?php
/** Template Name: Task */
get_header();?>
<div class="right_col" role="main">
	<!--  SECTION LIST NICK -->
	  <div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
			  <div class="x_title">
				<h2>Thêm task</h2>
				<ul class="nav navbar-right panel_toolbox">
				  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
				  <li><a class="close-link"><i class="fa fa-close"></i></a>
				  </li>
				</ul>
				<div class="clearfix"></div>
			  </div>
			  <div class="x_content">				
				<div class="list_services">
					<?php
						$file = (isset($_GET['tid']) && $_GET['tid'])?'update-task.php':'new-task.php';
						require get_template_directory() . '/parts/'.$file;
					?>
				</div>
			</div>
		  </div>
		</div>
	  </div>
	  <!-- /SECTION LIST NICK -->
</div>
<?php get_footer(); ?>
