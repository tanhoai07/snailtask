<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
 do_action('hr_action_init');
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<!-- Bootstrap -->
	<link href="<?php echo THEME_URI ?>/css/bootstrap.min.css" rel="stylesheet">
	<!-- Font Awesome -->
	<link href="<?php echo THEME_URI ?>/css/font-awesome.min.css" rel="stylesheet">
	<!-- FullCalendar -->
	<link href="<?php echo THEME_URI ?>/css/fullcalendar.min.css" rel="stylesheet">

	<!-- Custom styling plus plugins -->
	<link href="<?php echo THEME_URI ?>/css/custom.min.css" rel="stylesheet">
	<?php wp_head(); ?>
</head>
<?php
if(isset($_GET['dev'])){
	
	//var_dumps(get_tasks_employee(12));
	exit;
	
}

$user = get_user_by('id',MYID);
?>
<body <?php body_class(); ?>>
	<div class="container body">
		<div class="main_container">
			<div class="col-md-3 left_col">
				<div class="left_col scroll-view">
					<div class="navbar nav_title" style="border: 0;">
						<a href="<?php echo home_url(); ?>" class="site_title"><i class="fa fa-paw"></i> <span>Nail Media</span></a>
					</div>
					<div class="clearfix"></div>
					<!-- menu profile quick info -->
					<div class="profile clearfix">
						<div class="profile_pic">
							<img src="<?php echo get_wp_user_avatar_src($user->ID, 'medium'); ?>" alt="..." class="img-circle profile_img">
						</div>
						<div class="profile_info">
							<span>Welcome,</span>
							<h2><?php echo $user->display_name ?></h2>
						</div>
					</div>
					<!-- /menu profile quick info -->
					<br />
					<!-- sidebar menu -->
					<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
						<div class="menu_section">
							<h3>General</h3>
							<?php
							$role_class = 'role-'.$user->roles[0];
							wp_nav_menu( array(
								'theme_location' => 'main_menu',
								'menu_class'     => 'main_menu side-menu nav '.$role_class,
							) );
							?>
						</div>
					</div>
					<!-- /sidebar menu -->
					<!-- /menu footer buttons -->
					<div class="sidebar-footer hidden-small">
						<a data-toggle="tooltip" data-placement="top" title="Settings">
							<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
						</a>
						<a data-toggle="tooltip" data-placement="top" title="FullScreen">
							<span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
						</a>
						<a data-toggle="tooltip" data-placement="top" title="Lock">
							<span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
						</a>
						<a data-toggle="tooltip" data-placement="top" title="Logout" href="<?php echo  wp_logout_url( get_permalink( get_page_by_path( 'login' ) ) ); ?>">
							<span class="glyphicon glyphicon-off" aria-hidden="true"></span>
						</a>
					</div>
					<!-- /menu footer buttons -->
				</div>
			</div>
			<!-- top navigation -->
			<div class="top_nav">
				<div class="nav_menu">
					<nav>
						<div class="nav toggle">
							<a id="menu_toggle"><i class="fa fa-bars"></i></a>
						</div>

						<ul class="nav navbar-nav navbar-right">
							<li class="">
								<a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
									<img src="<?php echo get_wp_user_avatar_src($user->ID, 'medium'); ?>" alt=""><?php echo $user->display_name ?> <span class=" fa fa-angle-down"></span>
								</a>
								<ul class="dropdown-menu dropdown-usermenu pull-right">
									<li><a href="<?php echo get_page_link(get_page_by_path('user-profile')).'?uid='.MYID ?>"> Profile</a></li>
									<li><a href="<?php echo  wp_logout_url( get_permalink( get_page_by_path( 'login' ) ) ); ?>"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
								</ul>
							</li>
						</ul>
					</nav>
				</div>
			</div>
			<!-- /top navigation -->