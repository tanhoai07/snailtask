<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
 do_action('hr_action_init');
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="http://gmpg.org/xfn/11">
    <?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <?php endif; ?>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <!-- Bootstrap -->
    <link href="<?php echo THEME_URI ?>/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo THEME_URI ?>/css/font-awesome.min.css" rel="stylesheet">
    <!-- bootstrap-datetimepicker -->
    <link href="<?php echo THEME_URI ?>/css/bootstrap-datetimepicker.css" rel="stylesheet">
    <!-- Custom styling plus plugins -->
    <link href="<?php echo THEME_URI ?>/css/custom.min.css" rel="stylesheet">
    <?php wp_head(); ?>

</head>
<body class="login">
<?php 
$studio = get_the_author_meta( 'user_studio_meta', MYID );
$user = get_user_by('id',MYID);
?>
<div class="header_employees">
    <div class="logo"><?php echo ($studio)?get_post($studio)->post_title:''; ?></div>
    <div class="employees">
        <a href="javascript:void(0)" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            <?php echo get_avatar(MYID, 32 ); ?> <?php echo $user->display_name; ?>
        </a>
        <a href="<?php echo  wp_logout_url(get_page_link(get_page_by_path('login'))); ?>"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
    </div>
</div>